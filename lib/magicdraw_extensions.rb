# Order matters!
require 'magicdraw_extensions/meta_info.rb'
load 'magicdraw_extensions/constants.rb'
load 'magicdraw_extensions/project.rb'
load 'magicdraw_extensions/helpers.rb'
load 'magicdraw_extensions/ruby_extensions.rb'        # Previously extensions_Ruby.rb
load 'magicdraw_extensions/magic_draw_stereotypes.rb' # Previously extensions_MagicDraw_stereotypes.rb
load 'magicdraw_extensions/magic_draw_nav.rb'         # Previously extensions_MagicDraw_nav.rb
load 'magicdraw_extensions/treeDump.rb'
load 'magicdraw_extensions/ruby_extensions_fromHelper.rb'  # Previously rubyBuilderHelper/ruby_extensions.rb