require 'java'
require 'magicdraw_extensions/class_test'
require 'magicdraw_extensions/object_extensions'
require 'fileutils'


# Classes & modules explicity used here
java_import 'com.nomagic.magicdraw.actions.ActionsConfiguratorsManager'
java_import 'com.nomagic.magicdraw.actions.MDActionsCategory'
java_import 'com.nomagic.magicdraw.ui.browser.actions.DefaultBrowserAction'
java_import 'com.nomagic.magicdraw.ui.actions.DefaultDiagramAction'
java_import 'com.nomagic.magicdraw.ui.actions.DefaultActiveDiagramAction'
java_import 'com.nomagic.actions.AMConfigurator'
java_import 'com.nomagic.magicdraw.actions.DiagramContextAMConfigurator'
java_import 'com.nomagic.magicdraw.actions.BrowserContextAMConfigurator'
java_import 'com.nomagic.magicdraw.uml.DiagramTypeConstants'
java_import 'com.nomagic.magicdraw.uml.DiagramType'
java_import 'java.util.Arrays'
java_import 'javax.swing.JOptionPane'
java_import 'javax.swing.JScrollPane'
java_import 'javax.swing.JTextArea'
java_import 'java.awt.Color'

# These are commonly used as target_klasses (but not explicitly used here)
java_import "com.nomagic.uml2.ext.magicdraw.classes.mdkernel.Diagram"
java_import "com.nomagic.uml2.ext.magicdraw.classes.mdkernel.Package"
java_import "com.nomagic.uml2.ext.magicdraw.auxiliaryconstructs.mdmodels.Model" # The root "Data" package is actually a Model
java_import "com.nomagic.magicdraw.ui.dialogs.MDDialogParentProvider"
java_import 'com.nomagic.magicdraw.uml.symbols.PresentationElement'
java_import 'com.nomagic.uml2.ext.magicdraw.base.ModelObject'

# ========== Some constants ================

# == MagicDraw singleton classes ==

def_or_redef :APP, Java::ComNomagicMagicdrawCore::Application.getInstance

# MagicDraw calls a transaction a session. Some operations (such as creating model elements)
# can only be done in sesions. The current session is a singleton, so we don't need to pass it around.
def_or_redef :SESSION_MANAGER, Java::ComNomagicMagicdrawOpenapiUml::SessionManager

# getDialogParent in MainFrame has been deprectated.
# def dialogParent; APP.getMainFrame.getDialogParent; end  # Apparently the main frame is also not available when APP is initialized.
def dialogParent; MDDialogParentProvider.getProvider.getDialogParent; end 

def browser; APP.getMainFrame.getBrowser; end

def verify_prometheus(desc)
  raise "Prometheus constant is not defined #{desc}" unless Object.const_defined?(:Prometheus)
  raise "Prometheus constant does not correspond to an existing directory #{desc}" unless Dir.exist?(Prometheus)
end

# Note that: 
#  * '~/Prometheus' does not work
#  * $Promethus does not work, because globals are different in each jRuby thread
def_or_redef :Prometheus, ENV['PROMETHEUS'] || File.join(ENV['HOME'], 'Prometheus')
FileUtils::Verbose::mkdir_p(Prometheus) unless File.exist?(Prometheus)
verify_prometheus('in magic_draw_glue.rb')

def show_vars(where)
  puts "show_vars #{where}"
  puts "\tSESSION_MANAGER not nil: #{nil!=SESSION_MANAGER}"
  puts "\tRubyPluginAction.selections not nil: #{nil!=RubyPluginAction.selections}"
  puts "\tRubyPluginAction.selections.size: #{RubyPluginAction.selections.size}" if RubyPluginAction.selections
  puts "\tRubyPluginAction.selection not nil: #{nil!=RubyPluginAction.selection}"
  # puts "\t$LOAD_PATH is: "; $LOAD_PATH.each {|path| puts "\t\t#{path}" }
  puts "\t$CLASSPATH is "; $CLASSPATH.each {|path| puts "\t\t#{path}" }
  puts "\t----"
end

def action_category(mngr, cat_symbol)
  cat = mngr.getCategory(cat_symbol.to_s)
  return cat if cat
  cat = MDActionsCategory.new(cat_symbol.to_s, "")
  mngr.addCategory(cat)
  cat
end

# Executes a no-arg block wrapped in exception printing code.
# Returns the block result if there was no error.
# Returns failure_result if there was an error.
#         If failure_result=:_error_message, then insted return the error message.
def safe_eval(failure_result=:_error_message)
    answer = failure_result
    begin
      answer = yield
    rescue Exception => ex
      puts("=========================================")
      puts(ex.message)
      puts("\n\t" + ex.backtrace.join("\n\t") )
      if Warnings.list.any?
        puts
        puts "Warnings were:"
        puts "\n\t" + Warnings.list.join("\n\t")
      end
      puts("=========================================")
      answer = ex.message if :_error_message==failure_result
    end
    answer
end

module RubyConfigurator
  attr_accessor :rubyAction
  
  def initialize(rubyAction)
    super()
    self.rubyAction = rubyAction
    add_configurator
  end
  # returns an int
  def getPriority; rubyAction.getPriority; end
  # Subclass methods
  def configure(*args); raise "Subclass responsibility"; end
  def add_configurator; raise "Subclass responsibility"; end
  def remove_configurator; raise "Subclass responsibility"; end
end
  
# This class is anonymous in the Java version
class Ruby_Browser_Configurator
  
  include BrowserContextAMConfigurator
  include RubyConfigurator
  
  # Methods to add and remove this configurator from the manager
  def add_configurator
    ActionsConfiguratorsManager.getInstance.addContainmentBrowserContextConfigurator(self)
  end
  def remove_configurator
    ActionsConfiguratorsManager.getInstance.removeContainmentBrowserContextConfigurator(self)
  end
  
  # mngr is a ActionsManager, browser is a Tree.  returns void.
  def configure(mngr, browser)
    return unless rubyAction.canBeUsed
    action_category(mngr, rubyAction.cat_symbol).addAction(rubyAction)
  end
end


class Ruby_Diagram_Configurator
  include DiagramContextAMConfigurator
  include RubyConfigurator
  attr_accessor :diagram_pe

  # Methods to add and remove this configurator from the manager
  def add_configurator
    ActionsConfiguratorsManager.getInstance.addDiagramContextConfigurator(self.rubyAction.diagram_type, self)
  end
  def remove_configurator
    ActionsConfiguratorsManager.getInstance.removeDiagramContextConfigurator(self.rubyAction.diagram_type, self)
  end
  
  # mngr is a ActionsManager, diagram is a DiagramPresentationElement
  # selected is a list of selected PresentationElements, and requestor is a PresentationElement
  def configure(mngr, diagram, selected, requestor)
    @diagram_pe = diagram
    return unless rubyAction.canBeUsed
    action_category(mngr, rubyAction.cat_symbol).addAction(rubyAction)
  end
end

# This is how MagicDraw calls our plugin code. We define it in a module so it can be mixed into implementations of several MagicDraw specific interfaces.
# This represents an acton (implemented via action_proc) that has:
# * A action_name that MagicDraw displays as human readable text
# * A plugin_name (used to load runner.rb, and for printing debug messages).  Note that a single plugin can have multiple actions.
# * A category specified by cat_symbol (used to group actions together)
# * An optionsl list of target_klasses used to filter selections
module RubyPluginAction
  # Zero arg block can process RubyPluginAction.selection, RubyPluginAction.selections
  # If anything other than nil is returned, it is converted to a string which is shown in a dialog box.
  attr_accessor :action_proc
  # Classes that must be selected in order to enable the menu pick. If not specified, the action is applicable to anything.
  attr_accessor :target_klasses
  # String corresponding to the name of the folder within lib
  attr_accessor :plugin_name
  # String describing action. This is passed to superclass initialize, so could possibly be obtained by a superclass getter.
  attr_accessor :action_name
  # Specifies a category. group does not seem to work.
  attr_accessor :cat_symbol
  
  # This is intended to replace $SELECTION. For the sake of backwards compatability, the implementation temporarily uses $SELECTION.
  def self.selection; $SELECTION; end
  def self.selection=(model_object); $SELECTION=model_object; end
  
  # This is intended to replace $SELECTIONS. For the sake of backwards compatability, the implementation temporarily uses $SELECTIONS
  def self.selections; $SELECTIONS||=[]; end
  def self.selections=(model_objects); $SELECTIONS=model_objects; end
  
  
  def self.selected_presentation_elements; @selected_presentation_elements||= []; end
  def self.selected_presentation_elements=(presentation_elements); @selected_presentation_elements=presentation_elements; end
  def self.filtered_selected_presentation_elements; @filtered_selected_presentation_elements||= []; end
  def self.filtered_selected_presentation_elements=(presentation_elements); @filtered_selected_presentation_elements=presentation_elements; end
  
  def initialize(plugin_name, actionName, cat_symbol, *target_klasses, &action_proc)
    self.plugin_name = plugin_name
    self.target_klasses = target_klasses
    self.action_proc = action_proc
    self.action_name = actionName
    self.cat_symbol = cat_symbol
    super(actionID = '', actionName, stroke = nil, group = nil)
    register
  end
  
  # ====== Override is optional ===========
  
  def getPriority; AMConfigurator::MEDIUM_PRIORITY; end
  
  # Decides whether or not this plugin is applicable to the selected objects
  def canBeUsed
    # Plugins do not require a selection if target_klasses is empty
    # Else, check that a valid selection exists
    target_klasses.empty? || getSelections.any?
  end
  
  def setup_globals
    # $Proj can't be done when APP is determined, becasue the project may not have loaded yet.
    $Proj = APP.getProject
    # $Data looks like the root package of the project. 
    # Actually it is an instance of com.nomagic.uml2.ext.magicdraw.auxiliaryconstructs.mdmodels.Model
    $Data_Package = $Proj.getModel
    $Factory = $Proj.getElementsFactory # com.nomagic.uml2.impl.ElementsFactory
    # Used by action method to determine the current ruby action
    $RA = self
  end
  
  # This is a misnomer: it should really be called "performAction".
  # This is what actually does whatever the plugin needs to do.
  # returns void. evt is a java.awt.event.ActionEvent.
  #def actionPerformed(evt); raise "Subclass responsibility"; end
  def actionPerformed(evt)
    setup_globals
    if canBeUsed
      RubyPluginAction.selections = getSelections
      RubyPluginAction.selection = RubyPluginAction.selections[0]
      #show_vars('actionPerformed')
      # A few things (such as code/schema generation) are not transactional, but are treated as transactions anyway.
      SESSION_MANAGER.getInstance().createSession(action_name);
      message = safe_eval &action_proc
      # apply changes and add command into command history so it can be undone. 
      SESSION_MANAGER.getInstance().closeSession();
    else
      message = 'Sorry, there were no valid selected items for this plugin.'
    end
    return if 'CANCELLED'==message
    
    unless $non_interactive
      area = JTextArea.new(message.to_s)
      area.setRows(20)
      area.setColumns(80)
      area.setEditable(false)
      #area.setLineWrap(true) # Uncommenting this line will cause text to wrap
      pane = JScrollPane.new(area)
    
      JOptionPane.showMessageDialog(nil, pane )
    end
    
    message
  end
  
  # Register the plugin's configurator (RubyPluginAction) in the list of plugins
  def register
    # Call the subclass's configurator method to determine configurator type
    config = configurator
    plugins << config
    config
  end
  
  # Returns a (possibly empty) Array of items selected in browser tree.
  # Tree is not currently being used
  def getSelections
    selections = unfiltered_selections
    return selections if target_klasses.empty?
    # Filter by target_klasses
    selections.select{|o| target_klasses.detect {|targ| o.kind_of?(targ) } }
  end

  # ====== Subclass responsibilities (must override) ===========

  # Return configurator for self
  def configurator; raise "Subclass responsibility"; end
  
  # Return a (possibly empty) collection of items the action is to operate upon
  def unfiltered_selections; raise "Subclass responsibility"; end
  
end


# An implementation (good enough for most purposes) that uses a block passed to initialize.
class RubyAction < DefaultBrowserAction
  include RubyPluginAction
  
  def unfiltered_selections
    # getSelectedObjects can return nil
    getSelectedObjects || []
  end
  
  # Return a ruby browser configurator
  def configurator
    Ruby_Browser_Configurator.new(self)
  end
end

# RubyDiagramAction used to inherit from DefaultDiagramAction.
# Somewhere along the way, RubyDiagramAction seems to have stopped finding selected PresentationElements. 
# I did not notice this until MD 18.0, but it may have happened sooner. Anyway, DefaultActiveDiagramAction
# works in MD 18.0
class RubyDiagramAction < DefaultActiveDiagramAction # DefaultDiagramAction # DefaultActiveDiagramAction
  include RubyPluginAction
  attr_accessor :diagram_type
  
  # Diagram type added here from super
  # Diagram type is one of the constant fields from DiagramTypeConstants and determines which diagram types the plugin is valid for.
  #   An example is DiagramTypeConstants::UML_ANY_DIAGRAM which has the value "Any Diagram"
  def initialize(plugin_name, actionName, cat_symbol, diagram_type, *target_klasses, &action_proc)
    self.diagram_type = diagram_type
    super(plugin_name, actionName, cat_symbol, *target_klasses, &action_proc)
  end
  
  # This returns model elements rather than presentation elements.
  # That's because we are usually more interested in the model elements, and it's also consistent with RubyAction
  # If you want presentation elements, they are available from RubyPluginAction.selected_presentation_elements and RubyPluginAction.filtered_selected_presentation_elements
  def unfiltered_selections
    # getSelected returns presentation elements and is sometimes nil
    selected = getSelected || []
    RubyPluginAction.selected_presentation_elements = selected
    if target_klasses.empty?
      RubyPluginAction.filtered_selected_presentation_elements = selected
    else
      RubyPluginAction.filtered_selected_presentation_elements = selected.select {|pe| 
        model_object = pe.getElement
        target_klasses.detect {|targ| model_object.kind_of?(targ) }
        }
    end
    selected.collect{|pe| pe.getElement}
  end
  
  # Return a ruby diagram configurator
  def configurator
    Ruby_Diagram_Configurator.new(self)
  end
end

module RubyAdapter
  # Use instance methods to retrieve class variable
  def plugins; @@plugins ||= Array.new; end
  def plugins=(val); @@plugins = val; end
  
  # Intended to replace $MESSAGE. For the sake of backwards compatability, the implementation temporarily uses $MESSAGE
  def self.message; $MESSAGE||= 'No warnings'; end
  def self.message=(string); $MESSAGE=string; end
  
  # See note about loadpath.
  def action(symbol);
    RubyAdapter.message = 'No warnings' 
    $Action=symbol
    load("#{$RA.plugin_name}/runner.rb")
    RubyAdapter.message
  end

# Sometimes it's useful to document what doesn't work,
  # The following code does not work because it fails to identify any gems when it runs under MagicDraw (even though it identifies gems under jruby irb)
  # This code requires Gem::Specification, which is defined in rubygems.rb.  There are several of these in an rvm installation of jruby, and it's not clear which one is needed.
  # None of the following worked:
  # require '/Users/griesser/.rvm/gems/jruby-1.7.3@global/gems/bundler-1.3.5/lib/bundler/source/rubygems'   # While requiring, get "NameError: uninitialized constant Gem".
  # require '/Users/griesser/.rvm/rubies/jruby-1.7.3/lib/ruby/shared/rubygems'   # No error, but fails to find *ANY* gems
  # require '/Users/griesser/.rvm/repos/jruby/lib/ruby/shared/rubygems'          # No error, but fails to find *ANY* gems
  # require '/Users/griesser/.rvm/repos/jruby/src/jruby/kernel19/rubygems'       # No error, but fails to find *ANY* gems
  # require '/Users/griesser/.rvm/src/rubygems-1.8.24/lib/rubygems'              # No error, but fails to find *ANY* gems
  # def load_plugins
  #   # Load gems which are plugins. We do this by matching the gem name against /plugin\./
  #   # An alternative would be to find plugins that have meta_info.rb, load the meta_info.rb files, 
  #   # query the resulting module.  Matching the gem name is a lot easier.
  #   puts("\nLoading Ruby plugins...")
  #   plugin_specs = Gem::Specification.select {|spec| 
  #     is_plugin = nil != /plugin\./.match(spec.name) 
  #     puts "\tGem #{spec.name.inspect} is_plugin: #{is_plugin.inspect}"
  #     is_plugin
  #     }
  #   plugin_specs.each {|spec| 
  #     name = spec.name
  #     puts("\tLoading plugin #{name}.")
  #     # Need to
  #     #  * Figure out path to jars required by this plugin
  #     #  * add those jar paths to $CLASSPATH
  #     safe_eval { load (name + '.rb') }
  #     }
  #   puts("Done loading #{plugin_specs.size} Ruby plugins.\n")
  #   show_vars('Init')
  # end
  
  

  # NOTE that all the plugins share the same loadpath.
  #      This makes it possible to share code files, 
  #      but it also make name conflicts possible.
  #      To avoid this, we could store
  #      dependencies in the RubyAction, and use it to set
  #      the load path for each plugin/invocation.
  def load_plugins
    # Load gems which are plugins. We do this by extracting the plugin name from $LOAD_PATH
    puts("\nLoading Ruby plugins...")
    # find plugin names
    plugin_names = Array.new
    $LOAD_PATH.each {|path|
      match_data = /gems\/plugin\.([^-]+)-/.match(path)
      plugin_names << match_data[1] if match_data
    }
    plugin_names.each {|name|
      puts("\tLoading plugin #{name}")
      # Need to
      #  * Figure out path to jars required by this plugin
      #  * add those jar paths to $CLASSPATH
      safe_eval { load (name + '.rb') }
      }
    puts("Done loading #{plugin_names.size} Ruby plugins.\n")
    show_vars('Init')
  end
  
  

  
  def reset_ruby_plugins
    puts
    puts 'NOTE: Reset will not affect files that were loaded with "require"'
    puts '      Use "load" if you would like them to be reloaded here'
    puts '-----------======== Removing Plugins ========----------------'

    plugins.each do |plugin_configurator|
      plugin_configurator.remove_configurator
      puts " - Removing plugin: #{plugin_configurator.rubyAction.plugin_name}"
    end
    puts '-----------======== Reloading Plugins ========----------------'
    load_plugins
    puts '-----------======== Done ========----------------'
    puts
  end
end


