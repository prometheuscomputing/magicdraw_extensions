

# These are taken from com/prometheus/mapper/extensions_MagicDraw.rb

java_import "com.nomagic.uml2.ext.jmi.helpers.StereotypesHelper"

module Java::ComNomagicUml2ExtMagicdrawClassesMdkernel::Element
=begin
  # This more complicated version was used by schemaGen.
  # Return nil if the requested stereotype is not present.
  # Otherwise return a Java::ComNomagicUml2ExtMagicdrawMdprofiles::Stereotype
  def get_stereotype(stereotypeName)
    asi = getAppliedStereotypeInstance
    return nil unless asi
    stereotypes = asi.getClassifier
    stereotypes.select {|s| stereotypeName==s.getName }[0]
  end
=end
  # name is a Symbol or String.
  # Returns a Stereotype if the receiver has one.
  # See also stereotype?(name)
  def get_stereotype(name)
    StereotypesHelper.getAppliedStereotypeByString(self, name.to_s)
  end
  # Returns a collection of Stereotypes (not names)
  def get_stereotypes
    StereotypesHelper.getAllAssignedStereotypes([self]).to_a
  end
  # Returns names of applied stereotypes, as Symbols
  def stereotype_names
    get_stereotypes.collect {|s| s.getHumanName.sub('Stereotype ', '').to_sym }
  end
  # Returns a boolean, true if the reciever has the specifed stereotype.
  # Name can be a Symbol or String. 
  # To actually get the stereotype itself (for example, to get the value for a tag),
  # use instead get_stereotype(name)
  def stereotype?(name)
    nil!=get_stereotype(name)
  end
  # Returns nil or a Java::ComNomagicUml2ExtMagicdrawClassesMdkernel::Slot
  # Stereotype and tag names can be Symbols or Strings
  def get_tag(stereotypeName, tagName, createSlotIfNotExists=false)
    stereotype = get_stereotype(stereotypeName)
    return nil unless stereotype
    # Does not seem to be a way to get this directly from helper
    StereotypesHelper.getSlot(self, stereotype, tagName.to_s, createSlotIfNotExists) 
  end
  # Tested only on tags with String and integer values.
  # Tag values are actually arrays. This presumes we only want the first value in the array.
  # If you want all the values, use get_tag_values
  def get_tag_value(stereotypeName, tagName, on_missing=:fail_silently)
    tag = get_tag(stereotypeName, tagName)
    # STDERR.puts "***** Found #{tagName.inspect} tag: #{nil!=tag.inspect}"
    lit = tag && tag.getValue[0]
    answer = convert_tag_lit(lit)
    # STDERR.puts "coverted_lit: #{answer.inspect}"
    return answer unless answer.nil?
    return nil if :fail_silently==on_missing
    raise "Expected to find non-empty tag #{tagName.inspect} for stereotype #{stereotypeName.inspect}, but did not."
  end
  # Tags are instances of Java::ComNomagicUml2ExtMagicdrawClassesMdkernel::Slot.
  # They respond to getValue with a Java::ComNomagicUml2ExtJmiCollections::ModelList.
  # The list acts like an Array. The values can be:
  #      Java::ComNomagicUml2ExtMagicdrawClassesMdkernel::LiteralString (for String values)
  #      com.nomagic.uml2.ext.magicdraw.classes.mdkernel.LiteralInteger (for Integer values)
  #      com.nomagic.uml2.ext.magicdraw.classes.mdkernel.LiteralReal
  #      com.nomagic.uml2.ext.magicdraw.classes.mdkernel.LiteralBoolean
  # This converts one of these instances into a useable Ruby object.
  # Exmpty Strings are regarded as values that have not been specified, and rturn nil.
  # A nil argument returns nil.
  def convert_tag_lit(lit)
      if lit
        answer = case lit
        when Java::ComNomagicUml2ExtMagicdrawClassesMdkernel::LiteralBoolean
          lit.isValue
        when Java::ComNomagicUml2ExtMagicdrawClassesMdkernelImpl::ElementValueImpl
          # This case usually happens when encountering a default value for an enumeration (an enumeration literal).
          # See modelGen for an example of how this could be handled. -SD
          nil
        when Java::ComNomagicUml2ExtMagicdrawClassesMdkernelImpl::InstanceValueImpl
          # This case happens when processing tags found within MD's "UML Standard Profile"
          # Ignoring it for now.
          nil
        else
          lit.getValue
        end
        case answer
        when String
          return answer unless answer.empty?
        when nil; # fall through
        else
          return answer
        end
      end
      nil
  end
  def convert_tag_lits(lit_array)
    return [] unless lit_array
    lit_array.collect {|lit| convert_tag_lit(lit) }
  end
  # Tested only on tags with String and integer values.
  # Tag values are actually Java::ComNomagicUml2ExtJmiCollections::ModelList instances,
  # which behave a lot like arrays. This returns an Array (which may be empty). Does not return nil.
  # If you know the tag should contain a single value, use get_tag_value
  def get_tag_values(stereotypeName, tagName, on_missing=:fail_silently)
    tag = get_tag(stereotypeName, tagName)
    # puts "***** Found #{tagName.inspect} tag: #{nil!=tag.inspect}"
    if tag
      lits = tag.getValue
      return convert_tag_lits(lits)
    end
    return [] if :fail_silently==on_missing
    raise "Expected to find tag #{tagName.inspect} for stereotype #{stereotypeName.inspect}, but did not."
  end
  # Returns a hash of the form [tag_name_string => tag_value_array].
  # The hash will be empty if the stereotype is not present.
  # Use of this method is probably a premature optimization. I prefer get_tag_value or get_tag_values
  def get_tag_values_hash(stereotypeName, on_missing_stereotype=:fail_silently, on_missing_tag=:fail_silently, ignore_empty=true)
    stereotype = get_stereotype(stereotypeName)
    raise "Expected to find stereotype #{stereotypeName.inspect}, but did not." unless stereotype || :fail_silently==on_missing_stereotype
    answer = Hash.new
    return answer unless stereotype
    properties = stereotype.getOwnedAttribute || []
    properties.each {|prop| 
      tag_name = prop.getName
      values = get_tag_values(stereotypeName, tag_name, on_missing_tag)
      answer[tag_name]= values unless values.empty? && ignore_empty
      }
    answer
  end
  
  # returns a hash with all stereotype data
  def stereotypes_hash
    ret = {}
    names = self.stereotype_names
    names.each{|name| vals = self.get_tag_values_hash(name); ret[name.downcase.to_sym] = vals if vals && vals.any? }
    ret
  end
  
  # Remove all stereotypes
  def remove_stereotypes
    StereotypesHelper.removeStereotypes(self)
  end
  # Applies the specified stereotypes to the receiver.
  # An exception is raised if this can not be done.
  # Names are Symbols or Strings.
  def apply_stereotypes(*names)
    names.each do |name|
      name       = name.to_s
      stereotype = StereotypesHelper.getStereotype($Proj, name)
      raise "Failed to find stereotype <<#{name}>>" unless stereotype
      can_apply  = StereotypesHelper.canApplyStereotype(self, stereotype) 
      raise "Can not apply stereotype <<#{name}>> to element of type #{self.class.name}" unless can_apply
      StereotypesHelper.addStereotypeByString(self, name)
    end
  end
  
  def apply_stereotype(stereotype)
    can_apply = StereotypesHelper.canApplyStereotype(self, stereotype) 
    raise "Can not apply stereotype <<#{name}>> to element of type #{self.class.name}" unless can_apply
    StereotypesHelper.addStereotype(self, stereotype)
  end
    
  # Creates tag if necessary. Does not create stereotype. 
  # Stereotype and tag names can be Symbols or Strings.
  # tagValue can currently be String, Boolean, integer
  def set_tag_value(stereotypeName, tagName, tagValue)
    slot = get_tag(stereotypeName, tagName, true)
    # Creation if missing does not mean creating the Tag definition within the Stereotype: it means creating a slot for the tag. 
    raise "Expected tag #{tagName.inspect} to be created if missing" unless slot
    # getValue returns a Java::ComNomagicUml2ExtJmiCollections::ModelList.
    # getValue[0] is a Java::ComNomagicUml2ImplMagicdrawClassesMdkernel::LiteralStringImpl if the value of the tag is a UML String.
    val     = nil
    factory = $Factory # does not work: getProject.getElementsFactory
    case tagValue
      # Make an instance of an implmentor of a sub-interface of ValueSpecification.
      # I don't see what to be used for floats, so they will be represented by strings.
      when TrueClass, FalseClass
        val = factory.createLiteralBooleanInstance
        val.setValue(tagValue)
      when Fixnum, Bignum
        val = factory.createLiteralIntegerInstance
        val.setValue(tagValue)
      else
        val = factory.createLiteralStringInstance
        val.setValue(tagValue.to_s)
    end
    # Example at https://community.nomagic.com/post1816.html#p1816 instead does it this way:
    slot.getValue.add(val)
    # There is also a StereotypesHelper:
    #   StereotypesHelper.addSlotValue(slot, val, true)  # I'm guessing false means replace value at index 0
    puts ">>>>>>>>> Added tag #{tagName.inspect} with value #{tagValue.inspect}"
  end
  # Applies stereotype if necessary. tag_hash is of form {tag_name => tag_value}.  
  # Stereotype and tag names can be Symbols or Strings. Tag values are currently converted to Strings
  def apply_tags(stereotypeName, tag_hash)
    apply_stereotypes(stereotypeName) unless stereotype?(stereotypeName)
    tag_hash.each { |tagName, tagValue| set_tag_value(stereotypeName, tagName, tagValue) }
  end
  
  def apply_tag_with_value(stereotype, tag, value)
    slot = StereotypesHelper.getSlot(self, stereotype, tag, true, false)
    unless slot
      msg = ''
      msg << self.inspect + "\n"
      msg << self.getQualifiedName + "\n"
      msg << stereotype.getQualifiedName + "\n"
      msg << tag.getQualifiedName + "\n"
      msg << value.inspect + "\n"
      msg << "Stereotypes" + "\n"
      self.get_stereotypes.each { |st| msg << st.getQualifiedName  + "\n" }
      STDERR.puts msg
      # NOTE: This was being called and failing and was NOT resulting in a proper error in MagicDraw.  It was just exiting semi-gracefully.  I'm not entirely sure why.  Be careful. -MF
      raise "Slot failure!\n#{msg}"
    end
    val = nil
    case value
    # Make an instance of an implmentor of a sub-interface of ValueSpecification.
    # I don't see what to be used for floats, so they will be represented by strings.
    when TrueClass, FalseClass
      val = MagicDraw.factory.createLiteralBooleanInstance
      val.setValue(value)
    when Integer
      val = MagicDraw.factory.createLiteralIntegerInstance
      val.setValue(value)
    else
      val = MagicDraw.factory.createLiteralStringInstance
      val.setValue(value.to_s)
    end
    # Example at https://community.nomagic.com/post1816.html#p1816 instead does it this way:
    slot.getValue.add(val)
  end
end

module Java::ComNomagicUml2ExtMagicdrawAuxiliaryconstructsMdmodels::Model
  def version; get_tag_value(:options, :version, :fail_silently) || '0.0.0'; end
end
