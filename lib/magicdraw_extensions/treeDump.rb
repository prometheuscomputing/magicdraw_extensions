# Modified version for use with JRuby

require 'common/treeDump'

begin
  class Java::ComNomagicUml2ExtJmiCollections::UnionModelSet
    def treeDump(maxDepth=0, io=STDOUT, seen=Array.new, depth=0)
  	return self if __printHeader(maxDepth, io, seen, depth)
  	newDepth = depth+1
  	newLine = "\n" + ("\t" * newDepth)
    to_a.treeDump(maxDepth, io, seen, newDepth)
  	__printFooter(io, depth)
  	self
    end
  end
rescue
  puts "Unable to load Java::ComNomagicUml2ExtJmiCollections::UnionModelSet"
end

# cls is a Java class. Returns an Array of Java Method instances.
def get_all_methods(cls, getters_only=true)
  methods = Array.new
	current = cls
	while nil!=current
	  begin
	     methods << current.getDeclaredMethods
    rescue Exception
=begin
      # A best effort to get methods. Did encounter the following, but the error was not really in getDeclaredMethods after all, it was in invoke.
      java.lang.NoClassDefFoundError: com/io_software/catools/tas/marks/TASTypeHandle
	at java.lang.Class.getDeclaredMethods0(Native Method)
	at java.lang.Class.privateGetDeclaredMethods(Class.java:2365)
	at java.lang.Class.privateGetPublicMethods(Class.java:2488)
	at java.lang.Class.privateGetPublicMethods(Class.java:2498)
	at java.lang.Class.getMethods(Class.java:1406)
=end
    end
	  current = current.respond_to?(:getSuperclass) ? current.getSuperclass : nil
  end
  methods.flatten!
	# methods.collect {|m| m.getName }.sort.treeDump # Yes, this works (does not include inherited methods)
	args=nil
	# We set args to an empty getParameterTypes because I am not certain how to create a new empty Java Object[].
	methods = methods.select {|m| 0==m.getName.index('get') && 0==(args=m.getParameterTypes).length && 'getRepository'!= m.getName } if getters_only
	return methods, args
end

def get_all_method_names(cls, getters_only=true)
  methods, args = get_all_methods(cls, getters_only)
  methods.collect {|m| m.getName }
end

# obj is a Java object
def get_getter_values(obj)
	cls = obj.getClass # Expect this to be a Ruby proxy to a Java class
	methods, args = get_all_methods(cls)
	# fields = cls.getDeclaredFields # Would probably work, but fields are often private.
  hash = Hash.new
	methods.select {|m| # Appears to not like :each, but same error with :select, must be m.invoke not happy with [] as args
	  name = m.getName
	  begin
	      hash[name] = m.invoke(obj, args)
    rescue
       # Best effort.
    end
	  false
	  }
	hash
end

# ===================================

class Object

# Prints everhthing recursively reachable from the receiver
# and it's instance variables. Does not print class variables.
# maxDepth is the number of levels to print.
#	maxDepth<1  recureses infinitely deep
#	maxDepth=1  prints only the receiver (without instance variables)
#	maxDepth=2  prints receiver and instance variables, but not the 
#			attributes of the things in the instance variables
def treeDump(maxDepth=0, io=STDOUT, seen=Array.new, depth=0)
  the_name = self.class.name || '_NAMELESS_CLASS_??_' # Was getting "undefined method `index' for nil:NilClass" in MD 18.0 with jruby-1.7.16
  return j_dump(maxDepth, io, seen, depth) if 0==the_name.index('Java::') && 'Java::JavaObject'!=the_name
	return self if __printHeader(maxDepth, io, seen, depth)
	newDepth = depth+1
	newLine = "\n" + ("\t" * newDepth)
	complex = Hash.new
	# Easier to read if we print the simple values first.
	instance_variables.sort.each { |getter|
		value = self.instance_eval( getter )
		if value.__isSimple?
			io << newLine
			io << getter + '  '
			value.treeDump(maxDepth, io, seen, newDepth)
		else
			complex[getter]=value
			end
		}
	# Now print the structured objects.
	complex.each { |getter, value|
		io << newLine
		io << getter + '  '
		value.treeDump(maxDepth, io, seen, newDepth)
		}
	__printFooter(io, depth)
	self
end



def j_dump(maxDepth=0, io=STDOUT, seen=Array.new, depth=0)
  # return j_Array_dump(maxDepth, io, seen, depth) if 'Java::JavaArray'==self.class.name # Does not understand each_with_index
  return j_iterator_dump(maxDepth, io, seen, depth) if respond_to?(:iterator) || 'Java::ComNomagicUml2ExtJmiCollections::UnionModelSet'==self.class.name
	return self if __printHeader(maxDepth, io, seen, depth)
	return puts "\n\n============= #{self.class.name} =======\n\n" if !respond_to?(:getClass)
	newDepth = depth+1
	newLine = "\n" + ("\t" * newDepth)
	complex = Hash.new
	# Now for the content
  get_getter_values(self).treeDump(maxDepth, io, seen, newDepth)
	__printFooter(io, depth)
	self
end

def j_iterator_dump(maxDepth=0, io=STDOUT, seen=Array.new, depth=0)
	return self if __printHeader(maxDepth, io, seen, depth)
	newDepth = depth+1
	newLine = "\n" + ("\t" * newDepth)
	complex = Hash.new
	# Now for the content
	array = Array.new
	it = iterator
	array << it.next while it.hasNext
	array.treeDump(maxDepth, io, seen, newDepth)
	__printFooter(io, depth)
	self
end

def j_Array_dump(maxDepth=0, io=STDOUT, seen=Array.new, depth=0)
	return self if __printHeader(maxDepth, io, seen, depth)
	newDepth = depth+1
	newLine = "\n" + ("\t" * newDepth)
	each_with_index { |value, index|
		io << newLine + "[#{index.to_s}] = "
		value.treeDump(maxDepth, io, seen, newDepth)
		}
	__printFooter(io, depth)
	self
end

end