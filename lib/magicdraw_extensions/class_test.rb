# Verify that necessary classes are available.
# This was used during the migration from MagicDraw 14.0 to 15.5.
# We don't really need to contunue running this code, but it doesn't hurt, and might as well remain in place in case of futre API changes.
# 
# This list of classes came from File_Processing/scripts/search_patterns.rb
# with $SOUGHT = [/Java::[a-zA-Z0-9]+::[a-zA-Z0-9]+/, /com\.nomagic\.[a-zA-Z0-9\.]+[a-zA-Z0-9]/]

=begin
This successfully identified the following changes:
  MISSING CONSTANT: Java::ComNomagicUml2ExtJmi::ModelList
      # Shows up only in comments
      # This has become Java::ComNomagicUml2ExtJmiCollections::ModelList
  MISSING CONSTANT: Java::ComNomagicUml2ExtJmi::UnionModelSet
      # This has become Java::ComNomagicUml2ExtJmiCollections::UnionModelSet
  MISSING CONSTANT: Java::ComNomagicUml2OmgClassesKernel::Classifier
      com.nomagic.uml2.ext.magicdraw.classes.mdkernel Classifier
      Java::ComNomagicUml2ExtMagicdrawClassesMdkernel::Classifier
  MISSING CONSTANT: Java::ComNomagicUml2OmgClassesKernel::NamedElement
      com.nomagic.uml2.ext.magicdraw.classes.mdkernel  NamedElement
      Java::ComNomagicUml2ExtMagicdrawClassesMdkernel::NamedElement
  MISSING CONSTANT: Java::ComNomagicUml2OmgClassesKernel::Package
      com.nomagic.uml2.ext.magicdraw.classes.mdkernel  Package
      Java::ComNomagicUml2ExtMagicdrawClassesMdkernel::Package
  MISSING CONSTANT: com.nomagic.uml2.omg.classes.dependencies.Dependency
      com.nomagic.uml2.ext.magicdraw.classes.mddependencies.Dependency
      Java::ComNomagicUml2ExtMagicdrawClassesMddependencies::Dependency
  MISSING CONSTANT: com.nomagic.uml2.omg.classes.kernel.ValueSpecification
      com.nomagic.uml2.ext.magicdraw.classes.mdkernel.ValueSpecification
      Java::ComNomagicUml2ExtMagicdrawClassesMdkernel::ValueSpecification
  MISSING CONSTANT: com.nomagic.uml2impl.magicdraw.classes.mdinterfaces.Interface
      # In this exact form, shows up only in comments.
      # another test establishes that we still have Java::ComNomagicUml2ExtMagicdrawClassesMdinterfaces::Interface
=end

def test_constant(name)
  begin
    m = Kernel.module_eval(name)
    puts "NOT MODULE: #{name}" unless m.kind_of?(Module)
  rescue
    puts "MISSING CONSTANT: #{name}"
  end
end

test_constant 'Java::ComNomagicMagicdrawCore::Application'
test_constant 'Java::ComNomagicMagicdrawOpenapiUml::SessionManager'
test_constant 'Java::ComNomagicMagicdrawUmlSymbols::DiagramPresentationElement'
test_constant 'Java::ComNomagicMagicdrawUmlSymbols::PresentationElement'
test_constant 'Java::ComNomagicMagicdrawUmlSymbolsPaths::AssociationView'
test_constant 'Java::ComNomagicMagicdrawUmlSymbolsPaths::DependencyView'
test_constant 'Java::ComNomagicMagicdrawUmlSymbolsPaths::NoteAnchorView'
test_constant 'Java::ComNomagicMagicdrawUmlSymbolsShapes::ClassView'
test_constant 'Java::ComNomagicMagicdrawUmlSymbolsShapes::DiagramFrameView'
test_constant 'Java::ComNomagicMagicdrawUmlSymbolsShapes::InnerElementsCompartmentView'
test_constant 'Java::ComNomagicMagicdrawUmlSymbolsShapes::NoteView'
test_constant 'Java::ComNomagicMagicdrawUmlSymbolsShapes::PackageView'
test_constant 'Java::ComNomagicUml2ExtMagicdrawClassesMdinterfaces::Interface'
test_constant 'Java::ComNomagicUml2ExtMagicdrawClassesMdkernel::Element'
test_constant 'Java::ComNomagicUml2ExtMagicdrawClassesMdkernel::PrimitiveType'
test_constant 'Java::ComNomagicUml2ExtMagicdrawClassesMddependencies::Dependency'
test_constant 'Java::ComNomagicUml2ExtMagicdrawClassesMddependencies::Realization'
test_constant 'Java::ComNomagicUml2ExtMagicdrawClassesMdinterfaces::Interface'
test_constant 'Java::ComNomagicUml2ExtMagicdrawClassesMdinterfaces::InterfaceRealization'
test_constant 'Java::ComNomagicUml2ExtMagicdrawClassesMdkernel::Association'
test_constant 'Java::ComNomagicUml2ExtMagicdrawClassesMdkernel::Class'
test_constant 'Java::ComNomagicUml2ExtMagicdrawClassesMdkernel::DataType'
test_constant 'Java::ComNomagicUml2ExtMagicdrawClassesMdkernel::Diagram'
test_constant 'Java::ComNomagicUml2ExtMagicdrawClassesMdkernel::Enumeration'
test_constant 'Java::ComNomagicUml2ExtMagicdrawClassesMdkernel::Generalization'
test_constant 'Java::ComNomagicUml2ExtMagicdrawClassesMdkernel::InstanceSpecification'
test_constant 'Java::ComNomagicUml2ExtMagicdrawClassesMdkernel::LiteralString'
test_constant 'Java::ComNomagicUml2ExtMagicdrawClassesMdkernel::Package'
test_constant 'Java::ComNomagicUml2ExtMagicdrawClassesMdkernel::PackageImport'
test_constant 'Java::ComNomagicUml2ExtMagicdrawClassesMdkernel::PrimitiveType'
test_constant 'Java::ComNomagicUml2ExtMagicdrawClassesMdkernel::Property'
test_constant 'Java::ComNomagicUml2ExtMagicdrawClassesMdkernel::Slot'
test_constant 'Java::ComNomagicUml2ExtMagicdrawCompositestructuresMdports::Port'
test_constant 'Java::ComNomagicUml2ExtMagicdrawMdprofiles::Stereotype'
#test_constant 'Java::ComNomagicUml2OmgClassesKernel::Classifier'
test_constant 'Java::ComNomagicUml2ExtMagicdrawClassesMdkernel::Classifier'
# test_constant 'Java::ComNomagicUml2OmgClassesKernel::NamedElement'
test_constant 'Java::ComNomagicUml2ExtMagicdrawClassesMdkernel::NamedElement'
# test_constant 'Java::ComNomagicUml2OmgClassesKernel::Package'
test_constant 'Java::ComNomagicUml2ExtMagicdrawClassesMdkernel::Package'
test_constant 'Java::JavaAwt::Color'
test_constant 'Java::JavaAwt::Font'
test_constant 'com.nomagic.actions.AMConfigurator'
test_constant 'com.nomagic.magicdraw.actions.ActionsConfiguratorsManager'
test_constant 'com.nomagic.magicdraw.actions.BrowserContextAMConfigurator'
test_constant 'com.nomagic.magicdraw.actions.MDActionsCategory'
test_constant 'com.nomagic.magicdraw.openapi.uml.ModelElementsManager'
test_constant 'com.nomagic.magicdraw.openapi.uml.PresentationElementsManager'
test_constant 'com.nomagic.magicdraw.ui.browser.Tree'
test_constant 'com.nomagic.magicdraw.ui.browser.actions.DefaultBrowserAction'
test_constant 'com.nomagic.uml2.ext.jmi.helpers.ModelHelper'
test_constant 'com.nomagic.uml2.ext.jmi.helpers.StereotypesHelper'
test_constant 'com.nomagic.uml2.ext.magicdraw.classes.mdkernel.Class'
test_constant 'com.nomagic.uml2.impl.ElementsFactory'
test_constant 'com.nomagic.uml2.ext.magicdraw.auxiliaryconstructs.mdmodels.Model'
test_constant 'com.nomagic.uml2.ext.magicdraw.classes.mddependencies.Dependency'
test_constant 'com.nomagic.uml2.ext.magicdraw.classes.mdkernel.Diagram'
test_constant 'com.nomagic.uml2.ext.magicdraw.classes.mdkernel.Package'
# test_constant 'com.nomagic.uml2.omg.classes.dependencies.Dependency'
test_constant 'com.nomagic.uml2.ext.magicdraw.classes.mddependencies.Dependency'
# test_constant 'com.nomagic.uml2.omg.classes.kernel.ValueSpecification'
test_constant 'com.nomagic.uml2.ext.magicdraw.classes.mdkernel.ValueSpecification'
# test_constant 'com.nomagic.uml2impl.magicdraw.classes.mdinterfaces.Interface' # See above comment

puts "=== class_test.rb complete ==="