require_relative 'project'
module MagicDraw
  def self.get_all_packages_from(packages, collected_packages = [])
    packages = Array(packages) # so you can pass in either a single package or an array of packages
    # puts packages.inspect
    # puts "Packages: #{print_packages(packages)}"
    return nil if packages.nil?
    packages.each do |pkg|
      puts "pkg is an array" if pkg.is_a?(Array)
      next if collected_packages.include? pkg
      collected_packages << pkg
      # puts "All Packages after adding #{pkg.getName}: #{print_packages(collected_packages)}"
      if imported_packages = get_imported_packages_for(pkg)
        # puts "Imported Packages: #{print_packages(imported_packages)}"
        imported_packages.each do |ip|
          collected_packages = (collected_packages + get_all_packages_from(ip, collected_packages)).compact.uniq.flatten
          # puts "All Packages: #{print_packages(collected_packages)}"        
        end
      end
      child_packages = pkg.packages
      if child_packages.any?
        # puts "Children of #{pkg.getName} begun"
        added_packages = get_all_packages_from(child_packages, collected_packages)
        collected_packages = (collected_packages + added_packages).compact.uniq.flatten
        # puts "Children of #{pkg.getName} finished"
      end
      # collected_packages.each{|pkg| puts pkg.inspect}
      collected_packages = collected_packages.compact.uniq.flatten
      # puts "All Packages at end of adding #{pkg.getName}: #{print_packages(collected_packages)}"
    end
    collected_packages
  end
  
  def self.get_imported_packages_for(package)
    # Add any dependencies of this package
    imported_packages = []
    package.getPackageImport.each do |p|
      # puts "import is #{p.name}"
      imp_pkg = p.getImportedPackage
      imported_packages << imp_pkg unless imp_pkg.getName == 'UML2 Metamodel'
    end
    imported_packages
  end
  
  def self.find_package_named(quailified_name, pkgs = $SELECTIONS)
    all_pkgs = get_all_packages_from(pkgs).flatten
    pkg = all_pkgs.find { |pkg| pkg.getQualifiedName == quailified_name }
    # all_pkgs.collect { |pkg| pkg.getQualifiedName }.sort.each { |name| puts name } unless pkg
    pkg
  end
  
  # NOTE: Do not try to cache any of this stuff here.  It will cause problems.  You might try doing so in the client code so that you are sure that you are getting the instances associated with the project upon which you are doing operations. YMMV.
  def self.uml_standard_profile
    root_model.packages.find { |pkg| pkg.name =='UML Standard Profile' }
  end

  def self.uml2_metamodel
    uml_standard_profile.packages.find { |pkg| pkg.name =='UML2 Metamodel' }
  end
  
  def self.md_profile
    uml_standard_profile.packages.find { |pkg| pkg.name =='MagicDraw Profile' }
  end
  
  def self.md_profile_datatypes
    @md_profile_datatypes = {}
    datatypes_pkg = md_profile.packages.find { |pkg| pkg.name == 'datatypes' }
    datatypes_pkg.getPackagedElement.each { |e| @md_profile_datatypes[e.getName.downcase.to_sym] = e }
    @md_profile_datatypes
  end
  
  def self.uml_classes
    @uml_classes = {}
    uml2_metamodel.getPackagedElement.each { |e| @uml_classes[e.getName.downcase.to_sym] = e }
    @uml_classes
  end
  
  def self.uml_primitives
    @uml_primitives = {}
    primitives_pkg = uml2_metamodel.packages.find { |pkg| pkg.name == 'PrimitiveTypes' }
    primitives_pkg.getPackagedElement.each { |e| @uml_primitives[e.getName.downcase.to_sym] = e }
    @uml_primitives
  end
    
end

# Provide helper methods for Auxiliary UML package (as described in umm_adapter)
module MagicDrawAuxiliary
  def self.uml_package
    primitives_pkg = MagicDraw.root_model.packages.find { |pkg| pkg.name == 'UML'}
  end
  
  def self.primitives
    @primitives = {}
    uml = uml_package
    return @primitives unless uml
    uml_package.getPackagedElement.each { |e| @primitives[e.getName.downcase.to_sym] = e }
    @primitives
  end
end
