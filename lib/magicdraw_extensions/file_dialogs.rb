require 'java'
require_relative 'magic_draw_glue'

java_import 'javax.swing.JFileChooser'

# The file chooser remembers the last directory used if it is the same instance each time.
$File_Chooser = JFileChooser.new(Prometheus)

class String
  # This method is provided in ruby 3, but not 2.
  def end_with?(*suffixes)
    suffixes.any? {|suffix| self.rindex(suffix) == (size-suffix.size) }
  end
end

# Previously we needed to use 'com.prometheus.rubyAdapter.ConcreteFileFilter' instead of 'javax.swing.filechooser.FileFilter', 
# due to a JRuby bug which has been fixed.
# The problem was that we couldn't make a subclass of FileFilter: it was not recognized.
java_import 'javax.swing.filechooser.FileFilter'

class RubyFileFilter < FileFilter
  # description is an end-user visible string.
  # If filter block is provided, it must take one arg (an absolute file path) and return a Boolean.
  # If provided, patterns are Regexp or suffix strings (including the '.'). Patterns are ignored if a filter is provided.
  def initialize(description, *patterns, &filter)
    @description=description
    @filter=filter || default_filter(*patterns)
    super()
  end
  def getDescription; @description; end
  def accept(file); @filter.call(file.getAbsolutePath); end
  def default_filter(*patterns)
    lambda {|path|
      patterns.any? {|pattern| 
        case pattern
          when String ; path.end_with?(pattern)
          when Regexp ; pattern =~ path
        end
        }
      }
  end
end

# Takes an optional one-arg block which is invoked only if a file/directory is selected.
# Block arg is the selected absolute path.
# The return value is:
# * nil if there is an error or if no file/directory is selected
# * the block result if a slection is made, and a block is provided
# * the selected absolute path if no block is provided
# Options is a hash with
# * Key is a setter for an instance of JFileChooser
# * Value is value to be set. If a key starts with 'add' then an array should be passed.
#   in this case a reset method (obtained by replaing 'add' by 'reset') will be called,
#   and then the setter will be called for each component value.
# :setFileSelectionMode => either JFileChooser::DIRECTORIES_ONLY, JFileChooser::FILES_ONLY, JFileChooser::FILES_AND_DIRECTORIES
# :setAcceptAllFileFilterUsed => aBoolean
# :setFileHidingEnabled => aBoolean # If true, does not show hidden files
# :setFileFilter => instance of subclass of FileFilter (suggest RubyFileFilter)
# :addChoosableFileFilter => a single instance of subclass of FileFilter (suggest RubyFileFilter) or an array of such instances
# :setFileView => an instance of a subclass of ImageFileView
# :setAccessory => an instance of a class that inherits from JComponent (lets you add extra GUI widgets to dialog)
def show_file_dialog(operation, options={}, &handler)
  # Options might be different for different invocations
  # TODO: Fix use of global here. This is just asking for trouble, since not all file_choosers should apply the same restrictions (which options may contain). -SD
  options.each {|setter, value|
    next $File_Chooser.send(setter, value) unless 0==setter.to_s.index('add') && value._instance_of?(Array)
    r = setter.sub('add', 'reset')
    $File_Chooser.send(r)
    value.each {|val|
      $File_Chooser.send(setter, val)
      }
    }
  op = :open==operation ? :showOpenDialog : :showSaveDialog
  return_code =  $File_Chooser.send(op, dialogParent)
  case return_code
    when JFileChooser::CANCEL_OPTION  ; nil # do nothing
    when JFileChooser::ERROR_OPTION   ; puts "Dialog error"; nil
    when JFileChooser::APPROVE_OPTION
      path = $File_Chooser.getSelectedFile.getAbsolutePath
      handler ? handler.call(path) : path
    else raise "Unexpected return value from aJFileChooser.#{op}"
  end
end

# Takes an optional one-arg block which is invoked only if a file/directory is selected.
# Block arg is the selected absolute path.
# options are described int the comment for show_file_dialog
def show_open_dialog(options={}, &handler)
  show_file_dialog(:open, options, &handler)
end

# Takes an optional one-arg block which is invoked only if a file/directory is selected.
# Block arg is the selected absolute path.
# options are described int the comment for show_file_dialog
def show_save_dialog(options={}, &handler)
  show_file_dialog(:save, options, &handler)
end

=begin
# An example
def woof
  options = { 
    :setFileSelectionMode => JFileChooser::FILES_ONLY,
    :setFileFilter => RubyFileFilter.new('Twiggy files', '_functions.rb')
    }
  show_open_dialog(options ) {|path| puts "OPEN #{path}!"}
end
=end