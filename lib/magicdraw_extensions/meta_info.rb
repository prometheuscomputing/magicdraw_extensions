# must not have the word m-o-d-u-l-e above the next line (so that a Regexp can figure out the m-o-d-u-l-e name)
module Magicdraw_extensions
  
  # For more information about meta_info.rb, please see project MM, lib/mm/meta_info.rb
  
  # SUGGESTION: Treat "Optional" as meaning "can be nil", and define all constants, even if the value happens to be nil.
  
  # Required String
  GEM_NAME = "magicdraw_extensions"
  # Required String
  VERSION = '0.14.1'
  # Optional String or Array of Strings
  AUTHORS = ["A. Griesser"]
  # Optional String or Array of Strings
  EMAILS = ["a.griesser@prometheuscomputing.com"]
  # Optional String
  HOMEPAGE = nil
  # Required String
  SUMMARY = %q{Extends MagicDraw API}
  # Optional String
  DESCRIPTION = %q{This library makes the MagicDraw API easier to use and more object oriented. 
    For example, this adds extra methods to make it unnecessary to use MagicDraw 'helper' classes (such as StereotypesHelper).
    This is based on com.prometheus.extensions 0.4.1, with SHA1 id afa9bb9fcdd5d206d7a18d8328ed0327100190e9 (commited 3/15/2013). 
    Did not include lib/extensions/nist (because it contains only common, which is already a library, should never have been in extensions).
    Also did not include version.rb (and references to it), because it is deprecated in favor of meta_info.rb.}
  
  LANGUAGE = :ruby
  # This differs from Runtime version - this specifies the version of the syntax of LANGUAGE
  LANGUAGE_VERSION = ['>= 2.3']
  # This is different from aGem::Specification.platform, which appears to be concerned with OS.
  # This defines which implentation of Ruby, Java, etc can be used.
  # Required Hash, in same format as DEPENDENCIES_RUBY.
  # The version part is used by required_ruby_version
  # Allowable keys depend on LANGUAGE. They are in VALID_<language.upcase>_RUNTIMES
  RUNTIME_VERSIONS = {
    :jruby => ['>= 9.2']
  }

  APP_TYPES = [] # Should be empty
  LAUNCHER = nil # Should be nil
  
  # Optional Hashes - if nil, presumed to be empty.
  # There may be additional dependencies, for platforms (such as :maglev) other than :mri and :jruby
  # If JRuby platform Ruby code depends on a third party Java jar, that goes in DEPENDENCIES_JAVA
  DEPENDENCIES_RUBY = { :common => ['>= 0.1.1', '< 2.0.0'], :blankslate => '~> 3.1' }
  DEPENDENCIES_MRI = { }
  DEPENDENCIES_JRUBY = { }
  DEVELOPMENT_DEPENDENCIES_RUBY = { } 
  DEVELOPMENT_DEPENDENCIES_MRI = { }
  DEVELOPMENT_DEPENDENCIES_JRUBY = { }
  
  # Java dependencies are harder to handle because Java does not have an equivalent of Ruby gem servers.
  # The closest thing is Maven repositories, which are growing in popularity, but not yet ubiquitous enough to warrant supporting in this tool.
  # Currently only the keys are used, version info is ignored.
  # Keys can be the names of Jars (complete, including any version info embedded in name) that must be located in JARCHIVE (key must end in .jar), or the name of a constant (key must not end in .jar).
  # Constsants must be defined in this module. Constants must not be computed from absolute paths (use environmental variables if necessary).
  # Support for constants is provided primarily to accomodate MagicDraw, which requires a large number of jars.
  DEPENDENCIES_JAVA = { }
  DEPENDENCIES_JAVA_SE = { }
  DEPENDENCIES_JAVA_ME = { }
  DEPENDENCIES_JAVA_EE = { }
  
  # An Array of strings that YARD will interpret as regular expressions of files to be excluded.
  YARD_EXCLUDE = []
  
end