# Non-schema navigational code from schemaGen.rb (did include #roots, and #root? even though they are schema specific, because they are similar to other tests.)

require 'singleton'
require_relative 'object_extensions'

java_import 'java.util.ArrayList'
java_import "com.nomagic.uml2.ext.jmi.helpers.ModelHelper"
java_import "com.nomagic.uml2.ext.jmi.helpers.StereotypesHelper"

def_or_redef :INFINITY, 1.0/0

# This is a switch that selects how types are determined.
# If :STEREOTYPE, you can (for example) indicate the type of an enumeration through inheritance.
# If :MAGICDRAW, you can use the enumeration and primitive elements provided by MagicDraw.
# Currently SchemaGen is better tested with :STEREOTYPE while critique is better tested with :MAGICDRAW
$MODEL_TYPE = :STEREOTYPE # :MAGICDRAW :STEREOTYPE
def ster?; :STEREOTYPE==$MODEL_TYPE; end

def_or_redef :PRINT_PREFIX_WARNINGS, false
def_or_redef :PRINT_NAMESPACE_WARNINGS, false

$namespace_cache = Hash.new

# Used to extract content from <body> tag
DOC_PAT = /<body>([\s|\S]*)<\/body>/ unless defined?(DOC_PAT) # suppress warnings

$DOC_SRC_ORDER = [:md_documentation, :tag_documentation]

class Object
  # ==== Class based tests ====
  def is_class?; false; end
  def is_stereotype?; false; end
  def is_primitive?
    if self.respond_to?(:getQualifiedName)
      if self.getQualifiedName =~ /UML Standard Profile::MagicDraw Profile::datatypes::[a-z]/
        true
      else
        false
      end
    else
      false
    end
  end
  def is_enumeration?; false; end
  def is_singleton?; false; end
  def is_datatype?; false; end
  def is_interface?#; false; end
    kind_of?(Java::ComNomagicUml2ExtMagicdrawClassesMdinterfaces::Interface)
  end
  def is_package?; false; end
  def is_dependency?; false; end
  def isAssociationClass?; false; end
  def isAssociation?; false; end
  def is_property?; false; end
  def is_comment?
    kind_of?(Java::ComNomagicUml2ExtMagicdrawClassesMdkernel::Comment)
  end
end

class Array
  # Useful mainly for debuggeryging
  def getNames; collect {|element| element.getName }; end
end

#  Warnings are saved everywhere. We don't want that to be DDLGenerator specific.
#  "Warnings << aString"  replaces "DDLGenerator.warnings << aString unless DDLGenerator.warnings.include?(aString)"
#  Might want to use logger instead.
class Warnings
  include Singleton
  attr_accessor :list
  def initialize; self.list = Array.new; end
  def <<(warning); list << warning unless list.member?(warning); end
  def clear; list.clear; end
  def self.<<(warning); instance << warning; end
  def self.clear; instance.clear; end
  def self.list; instance.list; end
end

module Diagram_Operations

  # The Diagram itself does not directly have contents, as you can see by executing:
  #     RubyPluginAction.selection.owned_elements.each {|element| puts( "> "+element.class.name.inspect+" > "+element.ele_name)}
  # Instead it's the DiagramPresentationElement that has the contents.
  # You would think there would be a way to go from Diagram to DiagramPresentationElement,
  # but you need to ask the Project to get the DiagramPresentationElement for a given Diagram
  #diagramPrezElement = $Proj.getActiveDiagram
  # NOTE: RubyPluginAction.selection is a ModelObject. In this case it's expected to be a diagram from the containment tree (due to filtering by plugins that need Diagram_Operations)
  def diagramPrezElement; $Proj.getDiagram(RubyPluginAction.selection); end

  # Do not confuse with method on Element.
  # Apply one or more stereotypes to model elements associated with presentation elements
  # that are selected in the diagram. The existing sterotypes are removed if clear_old is true.
  # Names are Symbols or Strings.
  def apply_stereotypes(clear_old, *names)
    model_elements = diagramPrezElement.selected_modelElements
    model_elements.each {|me| 
      me.remove_stereotypes if clear_old
      me.apply_stereotypes(*names)
      }
  end

  # Do not confuse with method on PresentationElement.
  # 'Fill Color' => color_array.to_color
  # 'Pen Color' => color_array.to_color
  def set_properties(model_classes, hash)
      diagramPrezElement.selected_presentationElements.select_models(*model_classes).each {|pe|
          pe.set_properties(hash)
          }
  end

  # Show detailed info on selected presentation elements
  def describe_selected
        selected = diagramPrezElement.selected_presentationElements
        puts "Diagram has #{selected.size} presentation elements selected."
        selected.each {|pe|
          me = pe.getElement
          puts "PE: #{pe.class.name}  ME: #{me.class.name}  Properties:"
          pe.properties_hash(:strings).each {|key, value|
              puts "\tKey: #{key.inspect}   Value: #{value}"
              }
          # pe.treeDump(4)
          # me.treeDump(4)
          #pe.getUserText
          # pe.getChildrenWithSymbolProperties.treeDump(4) # no
          # pe.getSelected.treeDump(4) # no
          # puts "getCollections"; pe.getCollections.treeDump(4)
          # puts "getActiviteNoteAnchorView"; pe.getActiviteNoteAnchorView.treeDump(4)
          # puts "getConnectedPathElements"; pe.getConnectedPathElements.treeDump(4)  <<< yes, need to navigate
          #        returns list of Java::ComNomagicMagicdrawUmlSymbolsPaths::NoteAnchorView
          # puts "getElementsForLinkConnecting"; pe.getElementsForLinkConnecting.treeDump(4)
          # puts "getRelatedElement"; pe.getRelatedElement.treeDump(4)   <<<< yes, but only one element!
          #getPresentationElements
          #getRemovableChildren
          #getHeaderObject
          #getContainer
          #pe.getConnectedPathElements[0].getSupplier.treeDump(4)
          # pe.getConnectedPathElements[1].getSupplier.treeDump(4)
          puts pe.denotes_absent?.to_s
          pe.described.treeDump(4)
          }
  end


end # Diagram_Operations

module Enumerable
  # Selects elements that are kind_of? one of the arguments.
  # Returns the receiver if there are no arguments.
  # Java has many collection classes, some of which may not mix in Enumerable.
  # If that turns out to be the case, the best solution is probably to go ahead and mix in Enumerable (all it requires is #each)
  def filter_by_kindOf(*classes)
    return self if classes.empty?
    select {|item|
      classes.detect {|cls| item.kind_of?(cls)}
      }
  end
end

module Can_Implement_Interface
  def implemented_interfaces
    if self.respond_to?(:getInterfaceRealization)
      getInterfaceRealization.collect{|ir| ir.getContract}
    else
      implemented_ifaces = []
      deps = self.getClientDependency
      deps.each{|d| d.getSupplier.each{|s| implemented_ifaces << s if s.is_a?(Java::ComNomagicUml2ExtMagicdrawClassesMdinterfacesImpl::InterfaceImpl)}}
      implemented_ifaces
    end
  end
  
  # Returns a collection of Java::ComNomagicUml2ExtMagicdrawClassesMdinterfaces::Interface
  def __interfaces_implemented
    realizations = getClientDependency.select {|dep| 
      dep.kind_of?(com.nomagic.uml2.ext.magicdraw.classes.mdinterfaces.InterfaceRealization) && # This worked even with Realization because InterfaceRealization is a kind of Realization
        dep.getContract
      }
    realizations.collect {|r| r.getContract }
  end
end

module Cannot_Implement_Interface # yet...only because our generators don't handle them for things like enumerations
  # MagicDraw does not allow some things (such as Java::ComNomagicUml2ExtMagicdrawClassesMdkernel::Enumeration)
  # to implment Interfaces (MF -- Totally false...at least in v18 of MD.  I just did it).  As a result, they do not respond to getInterfaceRealization (MF -- now this is true, which means the MD API is sh1te).
  # Art thinks this is dead wrong: he sees no reason why an enumeration should not be required to adhere to a contract. (MF -- Agrees w/ Art. There is no reason why an enumeration shouldn't be able to implement an interface.)
  # Of course, that perspective comes from thinking of everything (including enumeration literals) as first class objects. (MF -- Literals are first class objects.  No question about it.)
  def implemented_interfaces
    []
  end
end

# Mixin defines association and attribute getter methods to supplement ones available through magicdraw
module AttributeAndAssociationMixin

  def getAllProperties(include_interface_attrs = true)
    # debuggery = true if self.name =~ /Car$|Vehicle$|Compact$|User$/
    # debuggery = true if self.name =~ /Car$/
    
    include_interface_attrs = false if is_interface? # Can't get implemented interfaces of an interface
    navigable_attributes = getNavigableAttributes(include_interface_attrs)
    
    # Get non-navigable attributes (and others that for some reason are not returned by getOwnedAttribute -- not sure why)
    others = []
    getAllAssociations(include_interface_attrs).each do |association, klass|
      # NOTE: getFirstMemberEnd is deprecated (and has a different signature) in MD 18.5, so fallback to checking
      #       the association's getMemberEnd properties
      end_one = ModelHelper.getFirstMemberEnd(association) rescue association.getMemberEnd[0]
      end_two = ModelHelper.getSecondMemberEnd(association) rescue association.getMemberEnd[1]
      if end_two.getType == end_one.getType
        # Then this is an association where both properties belong to the same class, i.e. a self-association
        others += [end_one, end_two]
      else
        # NOTE:  Must use getType on other_end because getOwner will not work with non-navigable ends
        this_end, other_end = end_two.getType.equal?(klass) ? [end_one, end_two] : [end_two, end_one]
        # append method '<<' doesn't work here
        others += [this_end]
      end
    end
    unique_others = others - navigable_attributes
    # puts "#{self.name} -- unique_others: #{unique_others.collect{|o| o.name}.inspect}" if unique_others.any? && debuggery
    properties = navigable_attributes | others
    # puts "#{self.name} -- properties: #{properties.collect{|o| o.name}.inspect}" if debuggery
    # puts if debuggery
    properties
  end
  
  # Attributes is a confusing term.  This arises due to the fact that MagicDraw treats attributes declared within a class and navigable association ends as both being attributes.  At the same time, MagicDraw makes a distinction between the association and the association ends.  The association itself is not considered an attribute.
  def getNavigableAttributes(include_interface_attrs)
    include_interface_attrs = false if is_interface? # Can't get implemented interfaces of an interface
    
    # debuggery = true if self.name =~ /Car$|Vehicle$|Compact$|User$/
    # debuggery = true if self.name =~ /Car$/
    # this isn't getting all attributes. It's missing ones that are non-navigable from the classifier's perspective.
    # owned_attrs = getOwnedAttribute.to_a
    # puts "#{self.name} -- owned_attrs: #{owned_attrs.collect{|a| a.name}.inspect}" if debuggery
    # this isn't getting all attributes either. It's missing ones that are non-navigable from the classifier's perspective. I haven't yet found a case where getAttribute returns something different than getOwnedAttribute.
    all_owned_attrs = getAttribute.to_a
    # puts "#{self.name} -- all_owned_attrs: #{all_owned_attrs.collect{|a| a.name}.inspect}" if debuggery
    # diff = all_owned_attrs - owned_attrs
    # puts "#{self.name} -- diff: #{diff.collect{|a| a.name}.inspect}" if diff.any?
        
    interface_attrs = include_interface_attrs ? _getInterfaceAttributes : []
    # puts "#{self.name} -- interface_attrs: #{interface_attrs.collect{|a| a.name}.inspect}" if debuggery && include_interface_attrs

    inherited_attrs = getInheritedAttributes.to_a
    # puts "#{self.name} -- inherited_attrs diff: #{(inherited_attrs - old_inherited_attrs).collect{|a| a.name}.inspect}" if debuggery
    
    attributes = (all_owned_attrs + interface_attrs + inherited_attrs).select{|attr| attr.kind_of?(Java::ComNomagicUml2ExtMagicdrawClassesMdkernel::Property)}
    # puts "#{self.name} -- attributes: #{attributes.collect{|a| a.name}.inspect}" if debuggery
    attributes
  end
  # alias_method :getAllAttributes, :getAllProperties
  # This method doesn't do exactly what it should but it has been left alone because it may be used in other places other than modelDriven_builder.  Really it should be fixed.
  def getAllAttributes(include_interface_attrs = true)
    Kernel.warn "getAllAttributes is deprecated in favor of getAllProperties.  Be aware that the behavior is different between the two methods and getAllProperties attempts to actually get ALL of the properties where getAllAttributes fails to do so."
    include_interface_attrs = false if is_interface? # Can't get implemented interfaces of an interface
    # this isn't getting all attributes....... It's missing ones that are non-navigable at one or both ends
    owned_attrs = getOwnedAttribute.toArray
    interface_attrs = include_interface_attrs ? getInterfaceAttributes : []
    # FIXME This doesn't get the attributes that are contributed by interfaces that parents realize.  Because of this, this method doesn't really get ALL properties.
    inherited_attrs = getInheritedMember.toArray
    attributes = (owned_attrs + interface_attrs + inherited_attrs).select{|attr| attr.kind_of?(Java::ComNomagicUml2ExtMagicdrawClassesMdkernel::Property)}
    # Get non-navigable attributes (and others that for some reason are not returned by getOwnedAttribute -- not sure why)
    getAllAssociations(include_interface_attrs).each do |association, klass|
      # NOTE: getFirstMemberEnd is deprecated (and has a different signature) in MD 18.5, so fallback to checking
      #       the association's getMemberEnd properties
      end_one = ModelHelper.getFirstMemberEnd(association) rescue association.getMemberEnd[0]
      end_two = ModelHelper.getSecondMemberEnd(association) rescue association.getMemberEnd[1]
      # NOTE:  Must use getType on other_end because getOwner will not work with non-navigable ends
      this_end, other_end = end_two.getType.equal?(klass) ? [end_one, end_two] : [end_two, end_one]
      # append method '<<' doesn't work here
      attributes += [this_end] unless attributes.any?{|attr| attr.equal?(this_end)}
    end
    attributes
  end
  
  def getDirectProperties
    # Note: this isn't getting all properties.  It's missing ones that are non-navigable from the classifier's perspective.
    properties = (getOwnedAttribute.toArray).select{|attr| attr.kind_of?(Java::ComNomagicUml2ExtMagicdrawClassesMdkernel::Property)}
    # Get non-navigable attributes (klass will always be self here)
    getDirectAssociations.each do |association, klass|
      # NOTE: getFirstMemberEnd is deprecated (and has a different signature) in MD 18.5, so fallback to checking
      #       the association's getMemberEnd properties
      end_one = ModelHelper.getFirstMemberEnd(association) rescue association.getMemberEnd[0]
      end_two = ModelHelper.getSecondMemberEnd(association) rescue association.getMemberEnd[1]
      # NOTE:  Must use getType on other_end because getOwner will not work with non-navigable ends
      this_end, other_end = end_two.getType.equal?(self) ? [end_one, end_two] : [end_two, end_one]
      # append method '<<' doesn't work here
      properties += [this_end] unless properties.include?(this_end)
    end
    properties
  end
  alias_method :getDirectAttributes, :getDirectProperties
  
  def getIndirectProperties
    raise
    # FIXME implement and use this and getDirectProperties as the components of getAllProperties
  end
  
  def getNavigableProperties
    raise
    # FIXME implement me.  This *should* be the same as getNavigableAttributes since the only other part of getAllProperties deal with getting non-Navigable (and a nebulous 'other') properties...but I'm not 100% sure. -MF
  end
  
  def getInterfaceAttributes
    implemented_interfaces.collect{|i| i.getAllAttributes(false)}.flatten
  end
  
  def _getInterfaceAttributes # FIXME bad name.  Really should replace getInterfaceAttributes but we can't do that without potentially breaking backwards compatibility.
    implemented_interfaces.collect{|i| i.getAllProperties(false)}.flatten
  end
  
  def getDirectAssociationAttributes
    associations_to_attributes(getDirectAssociations)
  end
  def getInheritedAssociationAttributes
    associations_to_attributes(getInheritedAssociations)
  end
  def getInterfaceAssociationAttributes
    associations_to_attributes(getInterfaceAssociations)
  end
  def getAllAssociationAttributes
    associations_to_attributes(getAllAssociations)
  end
  
  # Returns an array of [association, klass] arrays where klass is the class the association is defined on
  # (important for inherited associations)
  def getAllAssociations(include_interface_associations = true)
    # if debuggery = (self.name =~ /Car$|Vehicle$|Compact$|User$/)
    # if debuggery = (self.name =~ /Car$|Vehicle$/)
    # #     #getDirectAssociations
    #       owned_assocs = getDirectAssociations.collect{|p| p[0].name}
    #       puts "#{self.name} -- owned_assocs: #{owned_assocs.inspect}"
    # #     #getInheritedAssociations
    #       inherited_assocs = self.getInheritedAssociations.collect{|p| p[0].name}
    #       puts "#{self.name} -- inherited_assocs: #{inherited_assocs.inspect}"
    # #     #getInterfaceAssociations (switch)
    #       interface_assocs = self.getInterfaceAssociations.collect{|p| p[0].name} unless self.is_interface?
    #       puts "#{self.name} -- interface_assocs: #{interface_assocs.inspect}" unless self.is_interface?
    # end
    include_interface_associations = false if is_interface? # Can't get implemented interfaces of an interface
    getDirectAssociations + getInheritedAssociations(include_interface_associations) + (include_interface_associations ? getInterfaceAssociations : [])
  end
  def getDirectAssociations
    associations = Array.new
    ModelHelper.associations(self).each {|assoc| associations << [assoc, self]}
    associations
  end
  def getInheritedAssociations(include_interface_associations = true)
    associations = Array.new
    getParents.each{|gen|
      associations += gen.getAllAssociations(include_interface_associations)
    }
    associations
  end
  def getInheritedAttributes
    attributes = Array.new
    getParents.each{|generalization| attributes += generalization.getNavigableAttributes(true)}
    attributes
  end

  def getInterfaceAssociations
    associations = Array.new
    implemented_interfaces.collect{|int|
      associations += int.getAllAssociations(false) # Don't include assocs from implemented interfaces since we are an interface (which can't implement interfaces)
    }
    associations
  end
  
  def getAncestors
    ancestors = []
    getParents.each{|p| 
      ancestors << p
      ancestors += p.getAncestors
    }
    ancestors
  end

  def getParents
    parents = []
    getGeneral.each{|gen| 
      parents << gen
    }
    parents
  end
  
  private
  # Given an array of association,class pairs, converts them to properties
  def associations_to_attributes(associations)
    association_attributes = []
    associations.each do |association, klass|
      # NOTE: getFirstMemberEnd is deprecated (and has a different signature) in MD 18.5, so fallback to checking
      #       the association's getMemberEnd properties
      end_one = ModelHelper.getFirstMemberEnd(association) rescue association.getMemberEnd[0]
      end_two = ModelHelper.getSecondMemberEnd(association) rescue association.getMemberEnd[1]
      # NOTE:  Must use getType on other_end because getOwner will not work with non-navigable ends
      this_end, other_end = end_two.getType.equal?(klass) ? [end_one, end_two] : [end_two, end_one]
      association_attributes << this_end
      # Could be a self association, so check other_end
      if this_end.getType.equal?(klass)
        # Self association
        association_attributes << other_end
      end
    end
    association_attributes
  end
end # AttributeAndAssociationMixin

module Java::ComNomagicUml2ExtMagicdrawClassesMdkernel::Element
  
  def classifier?; kind_of?(Java::ComNomagicUml2ExtMagicdrawClassesMdkernel::Classifier); end
  # For:
  # * dependencies - the ends can be ports, attributes, and possibly other things.
  # * associations - I think the ends should only be classifiers
  def ends; [ModelHelper.getClientElement(self), ModelHelper.getSupplierElement(self)]; end
  # For dependencies and associations
  def classifiers; ends.collect {|ele| ele.classifier? ? ele : ele.getOwner }; end
  
  # Returns a possibly empty array of strings.
  # Not clear how to specify multiple documentation strings from the GUI, but the model clearly allows it.
  # Until we find out, better to use the #documentation method.
  def docs
    owned = getOwnedElement
    return [] unless owned
    comments = owned.select {|thing| thing.is_comment?}
    comments.collect {|comment| bod = comment.getBody.strip; DOC_PAT===bod ? $~[1].strip : bod}
  end
  # Returns nil or non-empty String.
  def md_documentation
    val = docs[0]
    return val if val && !val.empty?
    nil
  end
  # Returns nil or non-empty string.
  # Strips leading trailing whitespace. Then strips double quote if the string starts and ends with a double quaote.
  def tag_documentation
    val = get_tag_value(:Prometheus, :description, :fail_silently)
    return nil if !val || val.empty?
    val = val.strip
    val = val[1..-2] if val[0]=='"' && val[-1]=='"'
    return val if val && !val.empty?
    nil
  end
  # Returns a possibly empty String, never returns nil
  def documentation
    [:md_documentation, :tag_documentation].each {|getter|
      doc = send(getter)
      return doc if doc && !doc.empty?
      }
    String.new
  end
  # Presumes there is already an HTML comment: replaces it.
  def doc_body=(html)
    owned = getOwnedElement
    return [] unless owned
    comments = owned.select {|thing| thing.is_comment?}
    comments[0].setBody(html)
  end
  
  def is_facade_interface?; is_interface? && get_stereotype('facade'); end
  # Returns a two-element array: symbol and URI. Goes through cache.
  def __Namespace; $namespace_cache ||= {}; $namespace_cache[getID] ||= derive_namespace; end
  def derive_namespace; getPackage.__Namespace; end
  def get_uri; __Namespace[1]; end
  def get_prefix; __Namespace[0]; end
  #alias get_name getName
  
  def is_target?(uri)
    uri==getPackage.target_namespace[1]
  end
  
  def is_available?(uri)
    is_target?(uri) || getPackage.imported_namespaces.any? {|n| uri==n[1] } || "http://www.w3.org/2001/XMLSchema"==uri
  end
  
  def __Qname
    pkg = getPackage
    name = getName
    if pkg
      prefix, uri = pkg.__Namespace
      # Hack to always insert 'xsd' prefix.
      # TODO: Figure out how is_target? is really supposed to function, and fix it. -SD
      return "#{prefix}:#{name}" if prefix == 'xsd'
      is_target?(uri) ? name : "#{prefix}:#{name}"
    else
      Warnings << "Model element #{name} does not have a package"
      name
    end
  end
  
  # FIXME:  Really bad.  Pretty much arbitrary.  This should be on a per project basis and the valid prefixes should get set up during a configuration step or something.  Also, why would datatypes necessarily be primitives???  Answer: they wouldn't!
  def has_valid_primitive_prefix?
    primitive_prefixes = ["xsd", "PrimitiveTypes", "datatypes"] # valid primitive type prefixes
    primitive_prefixes.include? get_prefix
  end

  # Sorting is used to ensure reproducability
  def <=>(other); getName <=> other.getName; end
  def owning_package
    answer = self
    while answer
      answer = answer.getOwner
      return answer if answer.package?
    end
    nil
  end
  def owned_elements
    getOwnedElement.to_a
  end
  
  def all_elements(exclusions = [])
    oe = owned_elements.to_a
    oe.select! { |e| e.respond_to?(:getQualifiedName) }
    oe.reject! { |e| exclusions.find { |ex| e.getQualifiedName =~ /#{ex}/ } }
    (oe + (oe.collect { |e| e.all_elements(exclusions) if e.respond_to?(:all_elements) }) + (oe.collect { |e| e.getAllProperties if e.respond_to?(:getAllProperties) })
    ).flatten.uniq.compact
  end
  
  def owned_element(name)
    owned_elements.detect {|attr| name == attr.getName }
  end
  # Should work for Interfaces, Primitives, Enumerations. An override is used for Classes.
  def super_classifiers; getGeneral; end
  # Currently used only to test interface for 'extendible' attribute.
  def has_attribute_named?(name)
    getOwnedAttribute.any? {|attr| name == attr.getName }
  end
  # really only for interfaces
  def __is_extendable?
    # has_attribute_named?('extendable')
    nil != get_stereotype('extendable')
  end
  # Switchable
  def category_name
    case
        when class?       ; :Class
        when primitive?   ; :Primitive
        when enumeration? ; :Enumeration
        when interface?   ; :Interface
        when package?     ; :Package
        else :Unknown
    end
  end
  # ==== Class based tests - see class Object ====
  # ==== Stereotype based tests ====
  # NOTE: primitives and enumerations are either defined by inheritence (is_<type>?) or a stereotype
  def prim?; ((is_primitive? || ancestor_is_primitive?) && !is_enumeration?) || (is_class? && (stereotype?(:prim) || stereotype?('xsd_primitive'))); end
  #def prim?; is_class? && stereotype?(:prim); end
  def enum?; is_enumeration? || (is_class? && stereotype?(:enum)); end
  #def enum?; is_class? && stereotype?(:enum); end
  def datatype?; is_datatype? && !is_enumeration? && !is_primitive?; end
  def true_class?; is_class? && !(prim? || enum?); end
  def root?; is_class? && stereotype?(:Root) && true_class?; end
  # ==== Switchable tests ====
  def class?; ster? ? true_class? : is_class?; end
  def primitive?
    # __Qname is defined in schemaGen.rb
    return true if class? && 0==__Qname.index('http://www.w3.org/2001/XMLSchema:')
    if ster? 
      prim?
    else
      # puts "I am #{self.name}"
      ((is_primitive? || ancestor_is_primitive?) && !is_enumeration?)
    end
  end
  #def primitive?; ster? ? prim? : is_primitive?; end
  def enumeration?; ster? ? enum? : is_enumeration?; end
  # ==== Class based tests ====
  # The following did not function correctly:  "alias package?  is_package?"
  def package?; is_package?; end
  def interface?; is_interface?; end
  def dependency?; is_dependency?; end
  def property?; is_property?; end
  def inherits_from_primitive?
    primitive? || ancestor_is_primitive?
  end
  def singleton?; is_singleton?; end
  def ancestor_is_primitive?
    return false unless self.respond_to?(:getGeneral) && getGeneral && getGeneral.length > 0
    getGeneral.first.inherits_from_primitive?
  end

  # Return the lowest real primitive in the inheritance hierarchy
  def primitive_type
    if is_primitive? || (ster? && stereotype?(:prim))
      self
    else
      # This is allowing people to be sloppy and not denote (in the model) that a subclass of a primitive is also a primitive.
      if getGeneral && getGeneral.length > 0
        getGeneral.first.primitive_type
      else
        nil
      end
    end
  end

  # TODO: comment these
  def is_multiple?
    upper = getUpper
    if upper && upper.class == Fixnum
      upper != 0 && upper != 1
    else
      false
    end
  end

  def inverse_is_multiple?
    inverse_upper = getInverseUpper
    if inverse_upper && inverse_upper.class == Fixnum
      inverse_upper != 0 && inverse_upper != 1
    else
      false
    end
  end

  # These return nil if _get_inverse_multiplicity does
  # Should probably be changed to inverse_muliplicity
  def getInverseLower; mult = _get_inverse_multiplicity(self); mult[0] if mult; end
  def getInverseUpper; mult = _get_inverse_multiplicity(self); mult[1] if mult; end

  # Get the type of the enumeration literals
  # This method exists in Element since we are allowing enums to be specified by <<enum>> stereotype
  # as well as by class type
  def enumeration_type
    # First look in superclass for primitive ancestor
    prim_type = primitive_type if inherits_from_primitive?
    return prim_type if prim_type
  
    # Next look for attribute named 'type'
    type_attribute = getOwnedAttribute.select {|attribute| attribute.getName == 'type'}.first
  
    # Return type described by 'type' attribute or nil
    type_attribute ? type_attribute.getType : nil
  end
end # MD Element

# =================================================================================

# The root "Data" package is actually a Model (#category does return :Package)
# java_import "com.nomagic.uml2.ext.magicdraw.auxiliaryconstructs.mdmodels.Model"

# =================================================================================
module Java::ComNomagicUml2ExtMagicdrawAuxiliaryconstructsMdmodels::Model
  def is_package?; true; end
  def imported_packages; Array.new; end
  def importing_packages; Array.new; end
  # ==== Switchable content selection =======
  def classes;      owned_elements.select {|e| e.class? }; end
  def association_classes;      owned_elements.select {|e| e.isAssociationClass? }; end
  def primitives;   owned_elements.select {|e| e.primitive? }; end
  def datatypes;   owned_elements.select {|e| e.datatype? }; end # this does not include enums and primitives.
  def enumerations; owned_elements.select {|e| e.enumeration? }; end
  def interfaces;   owned_elements.select {|e| e.interface? }; end
  def entities;   owned_elements.select {|e| e.class? || e.primitive? || e.enumeration? || e.interface? }; end # added for change importer
  def packages;     owned_elements.select {|e| e.package? }; end
  def dependencies; owned_elements.select {|e| e.dependency? }; end # see also method on Java::ComNomagicMagicdrawUmlSymbols::PresentationElement
  def stereotypes; owned_elements.select {|e| e.is_stereotype? }; end
  def all; owned_elements; end
  def roots; classes.select {|cls| cls.root? }; end
end # MD Model

module Java::ComNomagicUml2ExtMagicdrawClassesMdkernel::Package
  def is_package?; true; end
  # Returns an Array of instances
  def imported_packages
    imp_class = Java::ComNomagicUml2ExtMagicdrawClassesMdkernel::PackageImport
    imps = owned_elements.select {|e| e.kind_of?(imp_class)}
    imps.collect {|imp| imp.getImportedPackage }
  end
  # Returns an Array of instances
  def importing_packages
    get_packageImportOfImportedPackage.collect {|i| i.getImportingNamespace }
  end
  # local_name is a symbol
  def element_named(local_name)
    owned_elements.detect {|e| e.responds_to?(:getName) && e.get_name.to_sym == local_name }
  end
  # ==== Switchable content selection =======
  def classes;      owned_elements.select {|e| e.class? }; end
  def association_classes;      owned_elements.select {|e| e.isAssociationClass? }; end
  def primitives;   owned_elements.select {|e| e.primitive? }; end
  def datatypes;   owned_elements.select {|e| e.datatype? }; end # TODO this would include enums and primitives.  should it???
  def enumerations; owned_elements.select {|e| e.enumeration? }; end
  def interfaces;   owned_elements.select {|e| e.interface? }; end
  def entities;   owned_elements.select {|e| e.class? || e.primitive? || e.enumeration? || e.interface? }; end # added for change importer
  # Contained packages
  def packages;     owned_elements.select {|e| e.package? }; end
  def dependencies; owned_elements.select {|e| e.dependency? }; end # see also method on Java::ComNomagicMagicdrawUmlSymbols::PresentationElement
  def stereotypes; owned_elements.select {|e| e.is_stereotype? }; end
  def all; owned_elements; end
  def imports?(pkg)
    sid = pkg.getID
    imported_packages.each {|p| return true if p.getID==sid }
    false
  end
  def roots; classes.select {|cls| cls.root? }; end

  def derive_namespace
    prefix = get_tag_value('XML Schema', 'prefix')
    uri = get_tag_value('XML Schema', 'targetNamespace')
    
    unless prefix && !prefix.empty?
      prefix = getName
      Warnings << "Package with name #{getName} id #{getID} does not have a prefix: using the name as the prefix" \
        if PRINT_PREFIX_WARNINGS
    end
    unless uri && !uri.empty?
      uri = getID
      Warnings << "Package with name #{getName} id #{getID} does not have a targetNamespace: using the id as the uri" \
        if PRINT_NAMESPACE_WARNINGS
    end
    [prefix, uri]
  end
  
  def class_exists?(class_name)
    classes.any? {|c| c.getName == class_name }
  end
  
  # Follow inheritance chain to get all packages contained by this one
  def all_contained_packages
    cp = Array.new
    # Add self
    cp << self
    # Iterate over children
    packages.each {|p| cp += p.all_contained_packages}
    cp
  end
  
  def target_namespace; __Namespace; end
  def imported_namespaces; imported_packages.collect {|p| p.__Namespace }; end
  
end # MD Package

module Java::ComNomagicUml2ExtMagicdrawClassesMdkernel::Classifier
  # Returns the Property at the tail end of unidirectional associations that point to the receiver.
  # I have not found a way in the MagicDraw API to iterate over such inbound associations, 
  # so we discover them by iterating over associated classes we find in some specified package(s).
  def inbound_properties(*pkgs)
    answer = Array.new
    pkgs = [self.owningPackage] if pkgs.empty? && owningPackage
    pkgs.each {|pkg|
      (pkg.classes + pkg.interfaces).each {|other|
        next if self == other
        other.getOwnedAttribute.each {|attribute| 
          is_expected = attribute.kind_of?(Java::ComNomagicUml2ExtMagicdrawClassesMdkernel::Property)
          answer << attribute if is_expected && attribute.getType==self && attribute.unidirectional? && attribute.navigable?
          }
        }
      }
    answer
  end
  # Returns an Array of (Arrays of qualifiers).
  # Each inner array contains qualifiers graphically represented on far end of one association with the receiver
  def pertinent_qualifiers(*pkgs)
    # Get properties on tail ends of unidirectional inbound associations
    properties = inbound_properties(*pkgs)
    # Add proporties on far ends of other associations
    getOwnedAttribute.each {|attribute| 
      is_expected = attribute.kind_of?(Java::ComNomagicUml2ExtMagicdrawClassesMdkernel::Property)
      next unless is_expected
      mp = attribute.mirror_property
      properties << mp if mp
      }
    # Get qualifiers from properties
    qualifiers = Array.new
    properties.each {|prop| 
      next unless prop.hasQualifier
      qualifiers << prop.qualifiers
      }
    qualifiers
  end
end # MD Classifier

module Java::ComNomagicUml2ExtMagicdrawClassesMdinterfaces::Interface
  
  def is_interface?; true; end
  
  def is_facade?; get_stereotype('facade'); end
  
  # Include methods getAllAttributes, getAllAssociations, getDirectAssociations, getInheritedAssociations
  include AttributeAndAssociationMixin
end

module Java::ComNomagicUml2ExtMagicdrawClassesMddependencies::Dependency
  def is_dependency?; true; end
end

module Java::ComNomagicUml2ExtMagicdrawClassesMdinterfaces::InterfaceRealization
    def <=>(other); getImplementingClassifier <=> other.getImplementingClassifier; end
end

module  Java::ComNomagicUml2ExtMagicdrawClassesMdkernel::Class
  def is_class?; true; end
  def super_classifiers; getSuperClass; end
  def literals; getOwnedAttribute; end
  # May be beneficial to move this to Element.
  def has_association?
    getOwnedAttribute.any? {|attr| 
      type = attr.getType
      type && ((type.class? && !type.primitive?) || type.interface?)
      }
  end

  def is_singleton?
    stereotype? "singleton"
  end
  
  # Include methods getAllAttributes, getAllAssociations, getDirectAssociations, getInheritedAssociations
  include AttributeAndAssociationMixin
  include Can_Implement_Interface
end

# This extends Java::ComNomagicUml2ExtMagicdrawClassesMdkernel::Class
module Java::ComNomagicUml2ExtMagicdrawMdprofiles::Stereotype
  def is_class?; false; end
  def is_stereotype?; true; end
end

module Java::ComNomagicUml2ExtMagicdrawClassesMdkernel::DataType
  def is_datatype?; true; end
  
  # Include methods getAllAttributes, getDirectAttributes, getAllAssociations, getDirectAssociations, getInheritedAssociations
  include AttributeAndAssociationMixin
  include Can_Implement_Interface
end

module Java::ComNomagicUml2ExtMagicdrawClassesMdkernel::PrimitiveType
  def is_primitive?; true; end
  
  # Include methods getAllAttributes, getDirectAttributes, getAllAssociations, getDirectAssociations, getInheritedAssociations
  include AttributeAndAssociationMixin
end

module Java::ComNomagicUml2ExtMagicdrawClassesMdkernel::Enumeration
  def is_enumeration?; true; end
  def literals; getOwnedLiteral; end
  
  # Include methods getAllAttributes, getDirectAttributes, getAllAssociations, getDirectAssociations, getInheritedAssociations
  include AttributeAndAssociationMixin
  include Can_Implement_Interface
end

module Java::ComNomagicUml2ExtMagicdrawClassesMdkernel::Property
  
  def directionality
    case
    when bidirectional?; :bidirectional
    when isNavigable; :toward
    else :awayFrom
    end
  end
  
  def direction_AB
    case
    when bidirectional?; :bidirectional
    when isNavigable; :toward_B
    else :toward_A
    end
  end
  
  def decoration
    # getAggregation returns an AggregationKindEnum
    got = getAggregation.to_s
    case got
      when 'none'; :none
      when 'shared'; :aggregation
      when 'composite'; :composition
      else raise "Unexpected result for getAggregation: #{got.inspect}"
    end
  end

  
  def _other_member_end
    # getAssociation ? getAssociation.getMemberEnd.detect {|prop| self!=prop} : nil  # Somehow {|prop| self!=prop}  does not work
    return nil unless getAssociation
    id = getID
    getAssociation.getMemberEnd.detect {|prop| id!=prop.getID} 
  end
  
  # Companion property at other end of association. Works when association is unidirectional.
  # Tests thus far show this returning nil only for Properties that represent Qualifiers
  def _mirror_property
    getOpposite || _other_member_end
  end
  
  # In the case of a directed AssociationClass, this is different from mirror_property.getOwningClass
  # (the association class itself is returnd by mirror_property.getOwnerClass, whereas this method
  # actually returns the classifier pointed to by the directed association class).  In the case of
  # a bidirectional association class, this behaves the same way as mirror_property.getOwnerClass
  def classifier_at_other_end
    candidate = ModelHelper.getClientElement(getAssociation)
    return candidate if candidate != getOwnerClass
    ModelHelper.getSupplierElement(getAssociation)
  end
    
  # If no block is provided, acts like _mirror_property
  # If block is provided:
  # * return the default unless there is a mirror property
  # * return the value of the block (executed in context of mirror property) if there is a mirror property
  def mirror_property(default=nil, &proc)
    opp = _mirror_property
    !proc ? opp : (!opp ? default : opp.instance_eval(&proc))
  end
  
  def inverse_multiplicity; mirror_property {mult}; end
  def inverse_navigable?; mirror_property(false) {isNavigable}; end
  def inverse_role_name(fallback=true); mirror_property {role_name(fallback)}; end
  def inverse_composite?; mirror_property(false) {isComposite}; end
  def inverse_derived?; mirror_property(false) {isDerived}; end
  def inverse_qualifiers; mirror_property([]) {qualifiers}; end
  
  def bidirectional?; isNavigable && inverse_navigable?; end
  def unidirectional?; !bidirectional?; end
  
  def mult; 
    max = getUpper
    max = INFINITY if max < 1
    Range.new(getLower, max)
  end
  
  def qualifiers; hasQualifier ? getQualifier.to_a : []; end
  
  # If fallback is true, this should never return nil: it will use the assocociated class name (with a warning issued).
  # If fallback is false, this will return nil if there is no role name or association name.
  # This now requires that association names do not start with numbers, and do not contain whitespace.
  #      That's so that this will work with associations that are weights for the Edmonds algorithm.
  #      The contstraint on whitespace may help filter out association names which are human readable comments.
  # Note that this does little processing of the role name.  SHOULD PROBABLY DO MORE.  Does not do pluralization. Currently, this can contain whitespace 
  #      (except when it comes from association name, which is more stringent), and the capitalization and pattern
  #      might not be suitable for some languanges.
  def role_name(fallback=true)
    name = getName
    name = name.strip if name
    return name if name && !name.empty?
    # fall back to association name
    name = getAssociation.getName if getAssociation
    name = name.strip if name
    return name if name && !name.empty? && /^\D\S*$/.match(name) # must be non-digit with no spaces
    # fall back to type name
    return nil unless fallback && getType
    typeName = getType.getName
    typeName = typeName.strip if typeName
    # warnings << "Association to #{typeName} has no role name or association name: using '#{typeName}'."
    typeName
  end
  
  def isAssociationClass?; 
    a = getAssociation
    nil!=a && a.isAssociationClass?
  end

  # Unfortunately, MD's getOwner will not return the owner of a directed association property.
  # Instead we will get the other end of the association and ask it its type.
  # This will always return the owning class of the property, or fail.
  def getOwnerClass
    owner = getOwner
    #TODO: consider never returning getOwner if getAssociation is non-nil (instead of type-checking getOwner)
    # This might be better since it is possible that an association can have properties and we may have wanted to
    # get the association as the owner of those properties.
    # NOTE: this is not done currently due to the ineffieciency of deriving the owner every call. If
    # no problems arise with the current solution, it's better for effieciency.
    return owner unless owner.nil? || 
      (owner.kind_of?(Java::ComNomagicUml2ExtMagicdrawClassesMdkernel::Association) && 
      !(owner.kind_of?(Java::ComNomagicUml2ExtMagicdrawClassesMdassociationclasses::AssociationClass)))
    association = getAssociation
    unless association.nil?
      # NOTE: getFirstMemberEnd is deprecated (and has a different signature) in MD 18.5, so fallback to checking
      #       the association's getMemberEnd properties
      test_end = ModelHelper.getFirstMemberEnd(association) rescue association.getMemberEnd[0]
      test_end_two = ModelHelper.getSecondMemberEnd(association) rescue association.getMemberEnd[1]
      other_end = test_end.equal?(self) ? test_end_two : test_end
      type = other_end.getType
      return other_end.getType # Using getType to be safe for directed associations
    end
    raise "Failed to derive owner of #{self.inspect} (got: #{owner.inspect}) #{('(name: ' + self.getName + ')') if self.respond_to?(:getName)}"
  end

  # Return the property on the opposite end of the association.
  # Deprecated - does not work under some circumsances.  Use mirror_property
  def getOppositeAssociationEnd
    _get_opposite_association_end(self)
  end
  def is_property?; true; end

  def get_role_name; _get_role_name(self, false); end
  def get_inverse_role_name; _get_inverse_role_name(self); end 
end

module Java::ComNomagicUml2ExtMagicdrawClassesMdkernel::Association
  # Check whether the given attribute is the first or second side of 
  # this association
  def side_a?(attribute)
    # NOTE: getFirstMemberEnd is deprecated (and has a different signature) in MD 18.5, so fallback to checking
    #       the association's getMemberEnd properties
    end_one = ModelHelper.getFirstMemberEnd(self) rescue self.getMemberEnd[0]
    attribute.eql?(end_one) ? true : false
  end
  
  def isAssociation?; true; end
  
  def _show_properties
    "getMemberEnd: "             + (hasMemberEnd         ? getMemberEnd.to_a.collect         {|prop| prop.getOwnerClass.getName} : nil).inspect +
    "    getOwnedEnd: "          + (hasOwnedEnd          ? getOwnedEnd.to_a.collect          {|prop| prop.getOwnerClass.getName} : nil).inspect +
    "    getNavigableOwnedEnd: " + (hasNavigableOwnedEnd ? getNavigableOwnedEnd.to_a.collect {|prop| prop.getOwnerClass.getName} : nil).inspect
  end
  def _desc
    a = ModelHelper.getClientElement(self)  # getFirstMemberEnd
    # a = a ? a.getClassifier : nil
    a = a ? a.getName : "?"
    b = ModelHelper.getSupplierElement(self) # getSecondMemberEnd
    # b = b ? b.getClassifier : nil
    b = b ? b.getName : "?"
    "#{a}  ----   #{b}"
  end
end

module Java::ComNomagicUml2ExtMagicdrawClassesMdassociationclasses::AssociationClass
  def is_class?; true; end
  def literals; getOwnedAttribute; end
  # May be beneficial to move this to Element.
  def has_association?
    getOwnedAttribute.any? {|attr| 
      type = attr.getType
      puts "MISSING TYPE FOR attr: #{attr.getName}  InClass: #{self.getName}" unless type
      type.class? || type.interface?
      }
  end
  
  # Include methods getAllAttributes, getDirectAttributes, getAllAssociations, getDirectAssociations, getInheritedAssociations
  include AttributeAndAssociationMixin
  include Can_Implement_Interface
  
  # Check whether the given attribute is the first or second side of 
  # this association
  def side_a?(attribute)
    # NOTE: getFirstMemberEnd is deprecated (and has a different signature) in MD 18.5, so fallback to checking
    #       the association's getMemberEnd properties
    end_one = ModelHelper.getFirstMemberEnd(self) rescue self.getMemberEnd[0]
    attribute.eql?(end_one) ? true : false
  end
  
  def isAssociationClass?; true; end
  def isAssociation?; true; end
  
end

# =================================================================================

# These property related methods were copied without from change on jan 4 2010
# from com.prometheus.mapper/extensions_MagicDraw.rb.  The mapper should be modified to use this.
class Java::ComNomagicMagicdrawUmlSymbols::PresentationElement
  # Returns a collection of Properties
  def properties
    getPropertyManager.getOwnProperties
  end
=begin
An example property hash, with value_type = :strings
	Key: "Path Style"   Value: "OBLIQUE"
	Key: "Show Conveyed Information A"   Value: "true"
	Key: "Show Conveyed Information B"   Value: "true"
	Key: "Rounded Corners"   Value: "false"
	Key: "Show Name"   Value: "true"
	Key: "Line Width"   Value: "1"
	Key: "Font"   Value: "Arial 11"
	Key: "Pen Color"   Value: "RGB [255, 0, 0]"
	Key: "Fill Color"   Value: "RGB [255, 255, 204]"
	Key: "Text Color"   Value: "RGB [0, 0, 0]"
	Key: "Show Tagged Values"   Value: "true"
	Key: "Wrap Words"   Value: "false"
	Key: "Constraint Text Mode"   Value: "EXPRESSION_MODE"
	Key: "Stereotype Font"   Value: "Arial 11"
	Key: "Use Fill Color"   Value: "false"
	Key: "Show Constraints"   Value: "true"
	Key: "Show Stereotypes"   Value: "false"
	Key: "Stereotype Color"   Value: "RGB [0, 0, 0]"
An example property hash, with value_type = :objects
	Key: "Path Style"   Value: String
	Key: "Show Conveyed Information A"   Value: TrueClass
	Key: "Show Conveyed Information B"   Value: TrueClass
	Key: "Rounded Corners"   Value: FalseClass
	Key: "Show Name"   Value: TrueClass
	Key: "Line Width"   Value: Fixnum
	Key: "Font"   Value: Java::JavaAwt::Font
	Key: "Pen Color"   Value: Java::JavaAwt::Color
	Key: "Fill Color"   Value: Java::JavaAwt::Color
	Key: "Text Color"   Value: Java::JavaAwt::Color
	Key: "Show Tagged Values"   Value: TrueClass
	Key: "Wrap Words"   Value: FalseClass
	Key: "Constraint Text Mode"   Value: String
	Key: "Stereotype Font"   Value: Java::JavaAwt::Font
	Key: "Use Fill Color"   Value: FalseClass
	Key: "Show Constraints"   Value: TrueClass
	Key: "Show Stereotypes"   Value: FalseClass
	Key: "Stereotype Color"   Value: Java::JavaAwt::Color
=end
  # Returns a Hash: key is Property name, value depends on value_type.
  # If value_type is :strings, value is a String representation of the Property value.
  # If value_type is :objects, value is the Property value object.
  def properties_hash(value_type=:strings, answer=Hash.new)
    properties.each {|p|
      val = :objects==value_type ? p.getValue : p.getValueStringRepresentation
      answer[p.get_name]=val
      }
    answer
  end
  # Returns a Property object
  def property(name)
    getPropertyManager.getPropertyByName(name)
  end
  # Returns a value Object
  def property_value(name)
    property(name).getValue
  end
  # Returns a String
  def property_s(name)
    property(name).getValueStringRepresentation
  end
  # Keys can in principle be symbols, but some have spaces, so might as well consistently use Strings.
  def set_property(name, value)
    raise "Trying to set property #{name.inspect} without value" unless nil!=value
    # The following changes the same property in many instances: 
    # it seems they are shared until they are changed by the GUI.
    # property(name).setValue(value)
    prop = property(name.to_s).clone # clone it because we don't want to know what subclass of Property to use.
    prop.setValue(value)
    getPropertyManager.apply([prop])
  end
  def set_properties(hash)
    hash.each {|key, value| set_property(key, value)}
  end
  # Filtered version of getPresentationElements.
  # getPresentationElements does not actually return model elements, it returns views, such as:
  #     Java::ComNomagicMagicdrawUmlSymbolsPaths::AssociationView
  #     Java::ComNomagicMagicdrawUmlSymbolsShapes::ClassView
  def child_views(*classes); getPresentationElements.filter_by_kindOf(*classes); end
  # Returns contained model elements (not presentation elements), possibly filtered by class
  def child_elements(*classes)
    elements = getPresentationElements.collect {|kid| 
      # puts "\t~~~ Kid type: #{kid.class.name}  Kid element type: #{kid.getElement.class.name}  Kid element name: #{kid.getElement.ele_name}"
      kid.getElement }
    elements.filter_by_kindOf(*classes)
  end
  def recursive_child_views(*classes)
    _collect_recursive_child_views.filter_by_kindOf(*classes)
  end
  # Answer does not include self.  Depth first traversal, on the subroutine stack.
  # PRESUMES THESE ARE A TREE - does not check for cycles
  def _collect_recursive_child_views(answer=Array.new)
    getPresentationElements.each {|pe|
      answer << pe
      pe._collect_recursive_child_views(answer)
      }
    answer
  end
    
  # ======= 
  # The following methods are intended primarily for use on DiagramPresentationElement.
  # They are placed here because they can operate at this level, and may eventually be handy.
  # =======
  # Gets child_elements that correspond to classes.
  def classes; child_elements(Java::ComNomagicUml2ExtMagicdrawClassesMdkernel::Class); end
  def associations; child_elements(Java::ComNomagicUml2ExtMagicdrawClassesMdkernel::Association); end
  def dependencies; child_elements(Java::ComNomagicUml2ExtMagicdrawClassesMddependencies::Dependency); end
  # Returns a collection of the selected PresentationElements (called "Symbols" in the GUI)
  # These are NOT model elements, they are instances of View classes (which are not documented).
  # For example a selected dependency is represented by an instance of
  #      Java::ComNomagicMagicdrawUmlSymbolsPaths::DependencyView
  def selected_presentationElements; getSelected.to_a; end
  def selected_modelElements; selected_presentationElements.collect {|pe| pe.getElement }.to_a; end
end

# =================================================================================

# TODO: these methods should be moved to attribute methods, and not require arguments

# If fallback is true, this should never return nil: it will use the assocociated class name (with a warning issued).
# If fallback is false, this will return nil if there is no role name or association name.
# Deprecated - use role_name
# The MagicDraw API often uses the word "attribute" insteaed of "property".  The first argument here is a Property.
def _get_role_name(attribute, fallback=true, allow_assoc_name=true)
  # First try the role name
  name = attribute.getName
  return name if name && !name.empty?
  # puts "~~~~~~~~~ role name #{name.inspect}, fallback = #{fallback.inspect}" unless allow_assoc_name
  # Fall back to association name, if allow_assoc_name
  name = attribute.getAssociation.getName if allow_assoc_name && attribute.getAssociation
  return name if name && !name.empty?
  return nil unless fallback
  # Fall back to the type name (from absurd workaround)
  # if !allow_assoc_name (because the association is an AssociationClass, and getType returns the AssociationClass instead of the other end)
  name = attribute.classifier_at_other_end.getName # This is essential for !allow_assoc_name (used when association is an association class). Should work in other cases as well
  # puts "~~~~~~~~~ mirror prop name #{name.inspect}" unless allow_assoc_name
  if name=='Has_Pet'
    puts attribute.to_s(label='Forward - looking for Feline', true)
    puts attribute.mirror_property.to_s(label='Mirror - looking for Feline', true)
  end
  return name if name && !name.empty?
  # Fall back to the type name (from getType, because I'm paranoid)
  name = attribute.getType.getName 
  # puts "~~~~~~~~~ getType  name #{name.inspect}" unless allow_assoc_name
  # warnings << "Association to #{name} has no role name or association name: using '#{name}'."
  name
end

# Given an association attribute for a class, it finds the role name for the reverse direction
# of the association
# Deprecated - use inverse_role_name
def _get_inverse_role_name(attribute)
  opposite_end = _get_opposite_association_end(attribute)
  opposite_end.nil? ? '' : _get_role_name(opposite_end, false)
  #opposite_end.getName if opposite_end && opposite_end.getName && opposite_end.getName != ''
end

# Given an association attribute, returns the attribute on the opposite end of the association
# Deprecated - does not work under some circumsances.  Use mirror_property instead.
def _get_opposite_association_end(attribute)
  #class_name = attribute.getOwner.getName
  return nil unless attribute.getAssociation
  # NOTE: getFirstMemberEnd is deprecated (and has a different signature) in MD 18.5, so fallback to checking
  #       the association's getMemberEnd properties
  association = attribute.getAssociation
  end_one = ModelHelper.getFirstMemberEnd(association) rescue association.getMemberEnd[0]
  end_two = ModelHelper.getSecondMemberEnd(association) rescue association.getMemberEnd[1]
  other_end = end_one.eql?(attribute) ? end_two : end_one
  other_end
end

# Given an association attribute, returns the multiplicity on the opposite end of the association
# Deprecated, use inverse_multiplicity
def _get_inverse_multiplicity(attribute)
  opposite_end = _get_opposite_association_end(attribute)
  [opposite_end.getLower, opposite_end.getUpper] if opposite_end
end


# TODO: needs to account for interfaces
def _sort_classes_by_inheritance(classes)
  sorted_classes = Array.new
  unsorted_classes = classes.clone
  
  while unsorted_classes.any?
    initial_num_unsorted = unsorted_classes.length
    good_to_go_classes, unsorted_classes = unsorted_classes.partition{|c|
      # Class is good to go if its superclass is already sorted, or its superclass is not found in passed classes
      c.getParents.inject(true){|ready, s| ready && (!classes.include?(s) || sorted_classes.include?(s))}
    }
    
    sorted_classes += good_to_go_classes
    if unsorted_classes.length == initial_num_unsorted
      # Problem detected. Did not pull out any classes this pass and unsorted_classes is still not empty.
      # Add the rest of the unsorted classes
      sorted_classes += unsorted_classes
      unsorted_class_names = unsorted_classes.collect{|c| c.getName}.join(", ")
      Warnings << "Could not sort all classes by inheritance. Superclass(es) for #{unsorted_class_names} not able to be sorted within #{unsorted_classes.first.getPackage.getName}."
      break
    end
  end
  
  sorted_classes
end

def _sort_interfaces_by_inheritance(interfaces)
  _sort_classes_by_inheritance(interfaces)
end

def _sort_datatypes_by_inheritance(datatypes)
  _sort_classes_by_inheritance(datatypes)
end

def _sort_primitives_by_inheritance(primitives)
  _sort_classes_by_inheritance(primitives)
end

def _sort_enumerations_by_inheritance(enums)
  _sort_classes_by_inheritance(enums)
end
# =================================================================================
  
# It appears this is no longer present in MagicDraw 17.02
# class Java::ComNomagicUml2ExtJmiCollections::UnionModelSet
#     def to_a
#       # This cumbersome transformation should not be necessary, 
#       # but JRuby has difficulty iterating over this type of collection.
#       # The following do return an Object JRuby can get the children from:
#       #    * toArray
#       #    * Java iteration:
#       #           array = Array.new
#       #           it = iterator
#       #           array << it.next while it.hasNext
#       all = ArrayList.new()
#       all.addAll self 
#       all.to_a
#     end
# end
