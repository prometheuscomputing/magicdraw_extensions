
class Object
  # Due to a JRuby bug, #instance_of? is currently broken - gives "wrong # of arguments(1 for 2)" 
  def _instance_of?(klass)
    self.class.name == klass.name
  end
end

class String
  # Extracts the file base name and extension from the receiver, and
  # prepends prefix, and makes that a path in the Prometheus/temp directory.
  # If this file exists, removes it. 
  def temp_file(prefix)
    name = split(File::SEPARATOR).pop
    temp_path =  File.join(Prometheus, 'temp', "#{prefix}_#{name}")
    require 'fileutils'
    FileUtils::Verbose::rm_f(temp_path)
    temp_path
  end
  
  ########
  # Extend string to add uncapitalize, recapitalize, camelize methods
  # TODO: should use pre-existing library for this.. Ruby Facets?
  def uncapitalize
    self.sub(/(^.)/) {|first_letter| first_letter.downcase }
  end
  def recapitalize
    self.sub(/(^.)/) {|first_letter| first_letter.upcase }
  end
  # def camelize # Not used atm
  #   self.split(/[^a-z0-9]/i).map{|w| w.capitalize}.join
  # end
  # prepend operator
  def >>(string)
    @value = string + self
  end

end

class Symbol
  # This is present mainly as a work around for the changed behavior of 
  # built-in #instance_variables, which returns different things under Ruby 1.8 and 1.9.
  # 1.8 returns Strings, 1.9 returns Symbols.
  def +(str); to_s + str.to_s; end
end

class Array
  def swap!(a,b)
    self[a], self[b] = self[b], self[a]
    self
  end
end

class Class
  def short_name
    name.split('::').last if name
  end
end
