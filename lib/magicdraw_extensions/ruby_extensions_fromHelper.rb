# This file is moved here, from rubyBuilderHelper/ruby_extensions.rb

class ::Object
  # gracenil - gracefully returns nil if any method in a method chain returns nil;
  # (Intead of undefined method ... for nil NilClass)
  def gracenil
    GraceNil.new(self)
  end
  # gracefail - gracefully returns nil if any method in a method chain is undefined or
  # returns nil 
  def gracefail
    GraceFail.new(self)
  end

  #NOTE: this is for debug only - can be removed
  def interesting_methods
    (self.methods - Object.new.methods).sort
  end

end

if defined?(BasicObject)
  superclass = BasicObject
else
  require 'blankslate'
  superclass = BlankSlate
end
class GraceNil < superclass
  def initialize(obj); @gracenil_object = obj; end
  def method_missing(method, *args, &block)
    return @gracenil_object if method == :end # if method was 'end' return object
    return nil.gracenil if @gracenil_object == nil # if object is nil, return wrapped nil
    @gracenil_object.send(method, *args, &block).gracenil
  end # end method_missing
end

class GraceFail < superclass
  def initialize(obj); @gracefail_object = obj; end
  def method_missing(method, *args, &block)
    return @gracefail_object if method == :end
    return nil.gracefail if @gracefail_object == nil # value is nil, return gracefail wrapper on it
    if @gracefail_object.respond_to?(method)
      @gracefail_object.send(method, *args, &block).gracefail
    else
      nil.gracefail # Object didn't respond to method. Return nil
    end
  end # end method_missing
end

class String
  # Similar to sequel's titleize method, but without destroying existing capitalization
  def to_title
    unless empty?
      # Go ahead and capitalize the first letter.  If not done then camel cased acronyms don't get formatted correctly (e.g. pMStore)
      sub(/\S/, &:upcase).
      # Parse camel casing to spaces
      gsub(/([a-z])([A-Z])/, '\1 \2').gsub(/([A-Z]+)([A-Z][a-z])/, '\1 \2').
      # Remove leading and trailing spaces and underscores
      gsub(/^[_ ]*/, '').gsub(/[_ ]*$/, '').
      # Parse underscoring to spaces
      gsub(/([A-Za-z])_([A-Za-z])/, '\1 \2').
      # Capitalize each word in the parsed string
      gsub(/\b([a-z])/){|x| x[-1..-1].upcase}
    end
  end
end