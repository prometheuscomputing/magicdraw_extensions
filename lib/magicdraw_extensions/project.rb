java_import "com.nomagic.magicdraw.core.Application"
java_import "com.nomagic.magicdraw.core.project.ProjectDescriptorsFactory"

module MagicDraw
  def self.magicdraw_instance
    # com.nomagic.magicdraw.core.Application
    Java::ComNomagicMagicdrawCore::Application.getInstance
  end
  class << self
    alias_method :application, :magicdraw_instance
    alias_method :instance,    :magicdraw_instance
  end
  
  def self.project
    magicdraw_instance.getProject
  end
  
  def self.factory
    project.getElementsFactory
  end
  
  def self.root_model # named 'Data' unless user changed it.
    project.getModel
  end
  
  def self.session_manager
    # com.nomagic.magicdraw.openapi.uml.SessionManager
    Java::ComNomagicMagicdrawOpenapiUml::SessionManager.getInstance
  end
  
  def self.create_session(str = "Prometheus Plugin")
    session_manager.createSession(str)
  end
  
  def self.close_session
    session_manager.closeSession
  end
  
  def self.project_for_model(md_model = root_model)
    Java::com.nomagic.magicdraw.core.Project.getProject(md_model)
  end
  
  def self.filename(md_model = root_model)
    project_for_model(md_model).getName
  end
  
  # from MagicDraw OpenAPI manual
  def self.save_project
    projectsManager   = Application.getInstance().getProjectsManager()
    project           = projectsManager.getActiveProject();
    projectDescriptor = ProjectDescriptorsFactory.getDescriptorForProject(project)
    projectsManager.saveProject(projectDescriptor, true);
  end
end
