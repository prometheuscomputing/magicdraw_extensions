# Adding to this as needed
# Just makes things nicer/prettier...
module MagicDraw
  COMPOSITE   = Java::ComNomagicUml2ExtMagicdrawClassesMdkernel::AggregationKindEnum::COMPOSITE
  SHARED      = Java::ComNomagicUml2ExtMagicdrawClassesMdkernel::AggregationKindEnum::SHARED
  PUBLIC      = Java::ComNomagicUml2ExtMagicdrawClassesMdkernel::VisibilityKindEnum::PUBLIC
  Model       = Java::ComNomagicUml2ExtMagicdrawAuxiliaryconstructsMdmodels::Model
  ModelImpl   = Java::ComNomagicUml2ExtMagicdrawAuxiliaryconstructsMdmodelsImpl::ModelImpl
  Package     = Java::ComNomagicUml2ExtMagicdrawClassesMdkernel::Package
  PackageImpl = Java::ComNomagicUml2ExtMagicdrawClassesMdkernelImpl::PackageImpl
  Profile     = Java::ComNomagicUml2ExtMagicdrawMdprofiles::Profile
  ProfileImpl = Java::ComNomagicUml2ExtMagicdrawMdprofilesImpl::ProfileImpl
  Element     = Java::ComNomagicUml2ExtMagicdrawClassesMdkernel::Element
  Classifier  = Java::ComNomagicUml2ExtMagicdrawClassesMdkernel::Classifier
  Interface   = Java::ComNomagicUml2ExtMagicdrawClassesMdinterfaces::Interface
  Association = Java::ComNomagicUml2ExtMagicdrawClassesMdkernel::Association
  AssociationClass = Java::ComNomagicUml2ExtMagicdrawClassesMdassociationclasses::AssociationClass
end
