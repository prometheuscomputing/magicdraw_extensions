class Object
  def def_or_redef(const, value)
    mod = self.is_a?(Module) ? self : self.class
    already_defined = mod.const_defined?(const)
    return if  already_defined && value == mod.const_get(const)
    # mod.send(:remove_const, const) if already_defined  # Do not need to remove old constant first
    mod.const_set(const, value)
  end
end


class String
  def to_class
    self.split(/\.|::/).inject(Kernel) {|scope, const_name| scope.const_get(const_name)}
  end
  
  # Capitalize first letter, leave the rest alone
  def capital
    self[0..0].upcase + self[1..length].to_s
  end
  
  # Replaces the current string with one that has had its first letter capitalized.
  def capital!
    # NOTE: jruby fails to correctly interpret self[0], and returns the Fixnum representation of the char
    #       Using a range forces jruby to return a String
    self[0] = self[0..0].upcase
    self
  end
  
  # Capitalize first letter, leave the rest alone
  def uncapital
    self[0..0].downcase + self[1..length].to_s
  end
  
  # Replaces the current string with one that has had its first letter capitalized.
  def uncapital!
    # NOTE: jruby fails to correctly interpret self[0], and returns the Fixnum representation of the char
    #       Using a range forces jruby to return a String
    self[0] = self[0..0].downcase
    self
  end
  
  # Remove whitespace, and apply appropriate capitalization
  def to_constant_name
    split(/\s/).collect{|substr| substr.capital }.join
  end
  
  # TODO: write and use these
  # # Remove whitespace and capitalization in favor of underscores
  # def to_underscores
  #   
  # end
  # 
  # # make name appropriate for ruby method
  # def to_ruby_method_name
  #   
  # end
  
  # make name appropriate for java method name
  def to_java_method_name
    to_constant_name.uncapital
  end
  
  # Replace the current string with a constant-name appropriate version
  def to_constant_name!
    replace(to_constant_name)
  end
  
  def escape_path
    gsub(/\s/, "\\\\\\&")
  end
end